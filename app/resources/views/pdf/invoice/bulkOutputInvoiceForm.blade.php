<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" href="https://jpafonts.osdn.jp/webfonts/jpafonts.css" />
    <link rel="stylesheet" href="{{ base_path() . '/public/css/pdf/invoice_form.css?v=1.2' }}">
    </link>
</head>
<body>
    @foreach ($invoice_form_datas as $datas)
    @php
        $page_num = 0;
        $total_page_num = 0;
        foreach ($datas as $data){
            foreach ($data['pages'] as $page){
                $total_page_num++;
            }
        }
    @endphp
    @foreach ($datas as $i => $data)
    <div class="del">
    @foreach ($data['pages'] as $j => $page)
    @php
        $page_num++;
    @endphp
    <div class="page">
        <div class="page_inner">
           <!-- <img class="company_seal" src="{{ base_path() . '/public/img/company_seal.png' }}" />-->
            <div class="heading">
                <table class="addressee">
                    <tbody>

                        <tr>
                            <td height="20"><span class="postal_code">〒{{$data['postal_code'] }}</span></td>
                        </tr>
                        <tr>
                            <td class="pl address" height="17">{{$data['first_adress']}}</td>

                        </tr>
                        <tr>
                            <td class="pl address" height="17">{{$data['second_adress'] }}</td>
                        </tr>
                        <tr>
                            <td class="pl atena" height="17">{{$data['name'] }} 御中</td>
                        </tr>
                        <tr>
                            <td class="pl atena" height="17">（{{$data['code'] }}）</td>
                        </tr>
                    </tbody>
                </table>
                <div class="metas_wrap">
                    <table class="metas">
                        <tbody>
                            <tr>
                                <div class="page_num">{{ $page_num }}/{{ $total_page_num }}</div>
                            </tr>
                            <tr>
                                <td colspan="2" height="20">
                                    <h1 class="title">御請求書</h1>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="date_of_issue" height="20">発行日：{{$data['today']  }}</td>
                            </tr>
                            <tr class="signature_postal_code">
                                <td colspan="2" height="20">〒652-0846</td>
                            </tr>
                            <tr class="signature_address">
                                <td colspan="2" height="20">神戸市兵庫区出在家町2丁目6番23号</td>
                            </tr>
                            <tr class="signature_contact">
                                <td height="20">TEL:078-651-2577</td>
                                <td height="20">FAX:078-651-2577</td>
                            </tr>
                            <tr class="signature_name">
                                <td colspan="2" height="20">有限会社　徳島屋</td>
                            </tr>
                            <tr class="signature_bank">
                                <td colspan="2" height="20">三菱UFJ銀行兵庫支店 普通0182710</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="total">
                <p>
                    毎度有難うございます。 下記の通りご請求申し上げます。
                </p>
                <p>
                    {{$data['invoice']->billing_date }}締め *(軽減税率対象8%)
                </p>
                <table class="total_table">
                    <tr>
                        <th>御入金額</th>
                        <th>繰越金額</th>
                        <th>今回御買上額</th>
                        <th>今回消費税額(内税)</th>
                        <th>今回消費税額(外税)</th>
                        <th class="no_border"></th>
                        <th>今回御請求額</th>
                    </tr>
                    <tr>
                        <td>0</td>
                        <td>0</td>
                        <td>{{ number_format($data['subtotal']) }}</td>
                        <td>{{ number_format($data['inner_tax']) }}</td>
                        <td>{{ number_format($data['outer_tax']) }}</td>
                        <td class="no_border"></td>
                        <td>{{ number_format($data['subtotal'] + $data['inner_tax']+$data['outer_tax']) }}</td>
                    </tr>
                </table>
            </div>
            <table class="invoice_detail">
                <thead>
                    <tr>
                        <th width="120">伝票日付</th>
                        <th width="100">伝票番号</th>
                        <th width="160">品名・規格</th>
                        <th width="50">数量</th>
                        <th width="50">単位</th>
                        <th width="60">単価</th>
                        <th width="120">金額</th>
                        <th width="">備考</th>
                    </tr>
                    <!--ここから１出荷-->
                    <!--出荷明細-->
                    @foreach ($page as $detail)
                    @if ($detail['type'] === 'shipment')
                    <tr class="shipment_total">
                        <th>{{ $detail['slip_date'] }}</th>
                        <!--明細1つ目のみ売上計上日-->
                        <th class="text_right">{{ $detail['slip_number'] }}</th>
                        @else
                    <tr>
                        <td>{{ $detail['slip_date'] }}</td>
                        <!--明細1つ目のみ売上計上日-->
                        <td class="text_right">{{ $detail['slip_number'] }}</td>
                        @endif

                        <!--一旦出荷番号 明細1つ目のみ-->
                        <td>{{ $detail['product_name'] }}</td>
                        <td class="text_right">{{ !is_numeric($detail['quantity'])?$detail['quantity']:number_format($detail['quantity']) }}</td>
                        <td>{{ $detail['unit'] }}</td>
                        <td class="text_right">{{ !is_numeric($detail['unit_price'])?$detail['unit_price']:number_format($detail['unit_price']) }}</td>
                        <td class="text_right">{{ is_numeric($detail['amount'])?number_format($detail['amount']):$detail['amount'] }}</td>
                        <td>{{ $detail['remarks'] }}</td>
                        <!--明細の備考ないので空白-->
                    </tr>
                    @endforeach

                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
    @endforeach
</div>
@endforeach
@endforeach
</body>

</html>
