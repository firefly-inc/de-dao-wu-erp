<html lang="jp">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" href="{{ base_path() . '/public/css/pdf/invoiceListForm.css' }}" />
    <title>請求一覧表</title>
    </link>
</head>

<body>
    @foreach ($dataList['pages'] as $page)
    {{--*/ $c =  $loop->index /*--}}
    <div class="page">
        <div class="header">
            <div class="left_container">
                <p class="serch">検索条件</p>
                <p class="billing_deadline">請求日:{{ $dataList['start_date'].'~'.$dataList['end_date'] }}</p>
            </div>
            <div class="right_container">
                <p class="print_date">印刷日:{{ \Carbon\Carbon::now()->format('Y/m/d') }}</p>
                </p>
                <p class="time">{{ \Carbon\Carbon::now()->format('H:i:s') }}</p>
                <p class="page">ページ: <span>{{ $loop->index+1 }}/{{ count($dataList['pages']) }}</span></p>
            </div>
        </div>
        <p class="ttl">請求一覧表</p>
        <table>
            <tbody>
                <tr>
                    <th>請求ID</th>
                    <th class="text-left">得意先名</th>
                    <th>前回請求金額</th>
                    <th>入金金額</th>
                    <th>繰越請求金額</th>
                    <th>売上金額</th>
                    <th>今回消費税額</th>
                    <th>税込売上金額</th>
                    <th>今回請求金額</th>
                </tr>
                @foreach ($page as $data)
                <tr>
                    <td>{{ $data['invoiceNo'] }}</td>
                    <td class="text-left">{{ $data['name'] }}</td>
                    <td>{{number_format((integer)$data['lastTimeAmount'])  }}円</td>
                    <td>{{ number_format((integer)$data['payment']) }}円</td>
                    <td>{{ number_format((integer)$data['kurikoshi']) }}円</td>
                    <td>{{ number_format((integer)$data['salesAmount'])}}円</td>
                    <td>{{ number_format((integer)$data['thisTimeTax'])}}円</td>
                    <td>{{ number_format((integer)$data['salesAmountTax'])}}円</td>
                    <td>{{ number_format((integer)$data['thisTimeInvoice'])  }}円</td>
                </tr>
                @endforeach
                @if(count($dataList['pages']) == $loop->index+1)
                <tr>
                    <td></td>
                    <td>合計</td>

                    <td>0円</td>
                    <td>{{ number_format((integer)$dataList['paymentSum']) }}円</td>
                    <td>{{number_format((integer)$dataList['kurikoshiSum'])  }}円</td>
                    <td>{{ number_format((integer)$dataList['salesAmountSum'])}}円</td>
                    <td>{{ number_format((integer)$dataList['thisTimeTaxSum'])}}円</td>
                    <td>{{ number_format((integer)$dataList['salesAmountTaxSum'])}}円</td>
                    <td>{{ number_format((integer)$dataList['thisTimeInvoiceSum'])  }}円</td>
                </tr>


                @endif
            </tbody>
        </table>
        <p class="company_mail">
            有限会社　徳島屋
        </p>
    </div>
    @endforeach
</body>

</html>
