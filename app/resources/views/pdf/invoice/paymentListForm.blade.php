<!DOCTYPE html>
<html lang="ja">

<head>
    <title>入金一覧表</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" href="{{ base_path() . '/public/css/pdf/paymentListForm.css?v=1.2' }}">
    </link>
</head>

<body>

    @foreach ($dataList['pages'] as $page)
    {{--*/ $c =  $loop->index /*--}}
    <div class="page">


        <div class="header">

            <div class="left_container">
                <table class="no_border">
                    <tbody>
                        <tr>
                            <td>検索条件</td>
                            <td>入金日付</td>
                            <td>
                                {{ $dataList['start_date'] }} ~{{ $dataList['end_date'] }}</td>
                        </tr>
                        <tr>
                            <td colspan="3">有限会社　徳島屋</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="right_container">
                <p class="print_date">印刷日:2021/07/19</p>
                </p>
                <p class="time">00:00</p>
                <p class="page">ページ: <span>{{ $loop->index+1 }}/{{ count($dataList['pages']) }}</span></p>
            </div>


        </div>
        <div class="center_container">
            <p class="ttl">入金チェックリスト</p>
        </div>

        <table class="data">
            <tbody>
                <tr>
                    <td>入金日付</td>
                    <td>伝票番号</td>
                    <td>得意先</td>
                    <td></td>
                    <td>件数</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>入金担当者</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>請求先</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>部門</td>
                </tr>
                <tr class="border">
                    <th></th>
                    <th>取引区分</th>
                    <th></th>
                    <th>入金金額　銀行</th>
                    <th>口座種別</th>
                    <th>手形番号</th>
                    <th>手記期日</th>
                    <th>摘要</th>
                    <th></th>
                </tr>

                @foreach ($page as $data)
                <tr>
                    <td>{{ $data['paymentDate'] }}</td>
                    <td>{{ $data['no'] }}</td>
                    <td>{{ $data['customerNo'] }}</td>
                    <td>{{ $data['customerName'] }}</td>
                    <td></td>
                    <td>{{ $data['subject'] }}</td>
                    <td></td>
                    <td></td>
                    <td>{{ $data['depositPerson'] }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>{{ $data['billingAddress'] }}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>{{ $data['department'] }}</td>
                </tr>
                @foreach ($data['detail'] as $dindex => $detail)
                <tr>
                    <td></td>
                    <td>{{ $detail['dealingsSection'] }}</td>
                    <td></td>
                    <td>
                        {{ empty($detail['PaymentAmount']) ? '' : number_format((integer)$detail['PaymentAmount']) . '円 ' }}
                        {{ empty($detail['bank']) ? '' : $detail['bank'] }}
                    </td>
                    <td>{{ $detail['account'] }}</td>
                    <td>{{ $detail['billsNo'] }}</td>
                    <td>{{ $detail['billsDeadline'] }}</td>
                    <td>{{ $detail['note'] }}</td>
                    <td></td>
                </tr>
                @endforeach
                <tr class="border">
                    <td></td>
                    <td>伝票合計</td>
                    <td></td>
                    <td>{{ number_format((integer)$data['slipTotal'] )}}円</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                @endforeach
                @if(count($dataList['pages']) == $loop->index+1)
                <tr>
                    <td></td>
                    <td>合計</td>
                    <td></td>
                    <td>{{ empty($dataList['total_amount']) ? '' : number_format((integer)$dataList['total_amount']) . '円'  }}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>


                @endif


            </tbody>
        </table>
        <p class="company_mail">
            有限会社　徳島屋
        </p>
    </div>
    @endforeach
</body>

</html>
