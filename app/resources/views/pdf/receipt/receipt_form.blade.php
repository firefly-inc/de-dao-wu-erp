<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>領収書</title>
    <link rel="stylesheet" href="{{ base_path() . '/public/css/pdf/receipt_form.css' }}">
    </link>
</head>

<body>
    <div class="first">
        <h1>
            領 収 証(控え)
        </h1>
        <div class="left">
            <span class="num">No. {{ sprintf('%010d', $data['invoice']->id) }}</span><br>

            <span class="acc_name"> {{ $data['invoice']->account->name }} 御中</span>
            <table class="main">
                <tr>
                    <td class="account_num">（ {{sprintf('%010d', $data['invoice']->account->id)}}）</td>
                    <td width="380" class="date">年 　　月　　 日</td>
                </tr>
                <tr>
                    <td class="deadline">{{ $data['invoice']->billing_date }}締切分</td>
                    <td class="amount">¥{{ number_format($data['invoice']->subtotal+$data['invoice']->tax) }} - </td>
                </tr>
                <tr>
                    <td></td>
                    <td class="message">上記金額正に領収いたしました<br>（消費税等込）</td>
                </tr>
                <tr>
                    <td></td>
                    <td class="img">
                        <img class="company_seal" src="{{ base_path() . '/public/img/title.png' }}" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td class="addres">
                        <p>本社/神戸市兵庫区出在家町2丁目6-23</p>
                        <p>Tel (078)651-2577 (代) </p>
                        <p>fax (078)681-7269 </p>
                    </td>
                </tr>
            </table>
        </div>
        <div class="right">
            <span class="bosyu">集金日　　  年　月　日</span>
            <table class="">
                <tr>
                    <td width="120">今月分</td>
                    <td width="80">{{ number_format($data['invoice']->subtotal) }}</td>
                </tr>
                <tr>
                    <td>消費税</td>
                    <td>{{ number_format($data['invoice']->tax) }}</td>
                </tr>
                <tr>
                    <td>今月請求額</td>
                    <td>{{ number_format($data['invoice']->subtotal+$data['invoice']->tax) }}</td>
                </tr>
                <tr class="no_b">
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>前々月繰越金額</td>
                    <td></td>
                </tr>
                <tr>
                    <td>前月繰越金額</td>
                    <td></td>
                </tr>
                <tr class="no_b">
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>合計請求額</td>
                    <td></td>
                </tr>
            </table>
            <table class="">
                <tr>
                    <td>現金</td>
                    <td width="120"></td>
                </tr>
                <tr>
                    <td>小切手</td>
                    <td></td>
                </tr>
                <tr>
                    <td>残金</td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="secand">
        <h1>
            領 収 証
        </h1>
        <div class="left">
            <span class="num">No. {{ sprintf('%010d', $data['invoice']->id) }}</span><br>

            <span class="acc_name"> {{ $data['invoice']->account->name }} 御中</span>
            <table class="main">
                <tr>
                    <td class="account_num">（ {{sprintf('%010d', $data['invoice']->account->id)}}）</td>
                    <td width="380" class="date">年 　　月　　 日</td>
                </tr>
                <tr>
                    <td class="deadline">{{ $data['invoice']->billing_date }}締切分</td>
                    <td class="amount">¥{{ number_format($data['invoice']->subtotal+$data['invoice']->tax) }} - </td>
                </tr>
                <tr>
                    <td></td>
                    <td class="message">上記金額正に領収いたしました<br>（消費税等込）</td>
                </tr>
                <tr>
                    <td></td>
                    <td class="img">
                        <img class="company_seal" src="{{ base_path() . '/public/img/title.png' }}" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td class="addres">
                        <p>本社/神戸市兵庫区出在家町2丁目6-23</p>
                        <p>Tel (078)651-2577 (代) </p>
                        <p>fax (078)681-7269 </p>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </div>

</body>

</html>
