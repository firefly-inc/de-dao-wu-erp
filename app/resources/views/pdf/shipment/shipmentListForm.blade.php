<html lang="jp">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Document</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<link rel="stylesheet" href="{{ base_path() . '/public/css/pdf/shipmentListForm.css?v=1.2' }}">
	<!--  AOS追加
	<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
	<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
	-->
	<!--<link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP:400,500,700" rel="stylesheet"> googlefontの追加例-->
</head>

<body>
@foreach ($dataList['pages'] as $page)
{{--*/ $c =  $loop->index /*--}}
<div class="page">
	<div class="header">
        <div class="right_container">
			<p class="print_date">印刷日:{{ \Carbon\Carbon::now()->format('Y/m/d') }}</p>
			</p>
			<p class="time">{{ \Carbon\Carbon::now()->format('H:i') }}</p>
			<p class="page">ページ: {{ $loop->index+1 }}/{{ count($dataList['pages']) }} <span></span></p>
		</div>
        <div class="center_container">
			<p class="ttl">担当者別販売実績表</p>
		</div>
		<div class="left_container">
			<table>
				<tbody>
					<tr>
						<td>担当者:</td>
						<td colspan="2">{{ $dataList['driver_name'] }}</td>
					</tr>
					<tr>
						<td>処理日付:</td>
						<td colspan="2">{{ $dataList['start_date'].'~'.$dataList['end_date'] }}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<table class="data">
		<tbody>
			<tr>
				<th width="120">得意先コード</th>
				<th width="360">得意先名</th>
				<th width="120">商品コード</th>
				<th width="240">商品名</th>
				<th width="120">出荷数</th>
                <th width=""></th>
			</tr>
			@foreach ($page as $data)
			<tr>
				<td width="">{{ $data['account_id'] }}</td>
				<td width="">{{ $data['account_name'] }}</td>
				<td width="">{{ $data['item_id'] }}</td>
				<td width="">{{ $data['item_name'] }}</td>
				<td width="">{{ $data['count'] }}</td>
                <td width=""></td>
			</tr>
			@endforeach
		</tbody>
	</table>
	<p class="company_mail">
		有限会社　徳島屋
	</p>
</div>
@endforeach
</body>

</html>
