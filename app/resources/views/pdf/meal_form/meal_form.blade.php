<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>食数確認表</title>
    <link rel="stylesheet" href="{{ base_path() . '/public/css/pdf/meal_num_form.css' }}">
    </link>
</head>

<body>
    <div class="inner">


        <h1 class="title">食数確認表</h1>
        <div class="heading">

            <table class="addressee">
                <tbody>
                    <tr>
                        <td height="20"><span class="account_name">{{ $data['account']->name }}御中
                                ({{ $data['account']->id }})★</span></td>
                    </tr>
                    <tr>
                        <td class="Deadline" height="20">{{ $data['end_date'] }}締め</td>

                    </tr>
                    <tr class="term">
                        <td height="20">対象期間:{{ $data['start_date'] }} ~ {{ $data['end_date'] }}</td>
                    </tr>
                </tbody>
            </table>
            <div class="metas_wrap">
                <p>印刷日 :{{$data['today']  }}</p>
                <table class="metas">
                    <tbody>
                        <tr>
                            <td class="message" height="20">
                                いつもお世話になっております
                            </td>
                        </tr>
                        <tr>
                            <td class="message" height="20">
                                食数確認をお願いいたします
                            </td>
                        </tr>
                        <tr>
                            <td class="from" height="20">
                                有限会社 徳島屋
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>

        <table class="meal_num">
            <thead>
                <tr>
                    <th rowspan="2" width="270">納品先名</th>
                    <th rowspan="2" width="270">商品</th>
                    <th rowspan="2" width="120">単価</th>
                    @foreach ($data['dates'] as $date)
                    <th width="30">{{ $date['day'] }}</th>
                    @endforeach
                    <th rowspan="2" width="50">合計</th>
                </tr>
                <tr>
                    @foreach ($data['dates'] as $date)
                    <th width="30">{{ $date['day_of_week'] }}</th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach ($data['detail'] as $d)
                @foreach ($d['item_list'] as $item)
                <tr>
                    @if ($loop->first)
                    <td rowspan="{{ count($d['item_list']) }}">{{ $d['name']}}</td>
                    @endif
                    <th>{{ $item['name'] }}</th>
                    <th>{{ $item['unit_price'] }}</th>
                    @foreach ($item['date_list'] as $day)
                    <th>
                        @if ($day>0)
                        {{ $day }}
                        @endif
                    </th>
                    @endforeach
                    <th>{{  $item['total']  }}</th>
                    @endforeach
                    @endforeach
                </tr>
            </tbody>

        </table>
    </div>
</body>

</html>
