<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['api']], function () {

    // 会員登録
    Route::post('/register', 'Auth\RegisterController@register')->name('api.register');
    // ログイン
    Route::post('/login', 'Auth\AuthController@login');
    // ログアウト
    Route::post('/logout', 'Auth\LoginController@logout')->name('api.logout');
    // ログインユーザー
    Route::get('/user', function () {
        return Auth::guard('api')->user();
    })->name('user');

    Route::get('/me', 'Auth\AuthController@me');

    //取引先
    Route::get('/account/meta', 'Account\AccountController@getMeta');
    Route::get('/account/list', 'Account\AccountController@getList');
    Route::get('/account/search/{word}', 'Account\AccountController@search');
    Route::get('/account/get/{id}', 'Account\AccountController@index');
    Route::get('/account/getPriceList/{id}', 'Account\AccountController@getPriceList');
    Route::get('/account/getDeliveryDestinationsList/{id}', 'Account\AccountController@getDeliveryDestinationsList');
    Route::get('/account/search', 'Account\AccountController@searchAccount');
    Route::get('/account/invoiceList/{id}', 'Account\AccountController@getInvoiceList');
    Route::get('/account/shipmentList/{id}', 'Account\AccountController@getShipmentList');
    Route::post('/account/save', 'Account\AccountController@save');
    Route::post('/account/post', 'Account\AccountController@post');
    Route::delete('/account/delete/{id}', 'Account\AccountController@delete');

    //取引先別単価
    Route::get('/CustomerUnitPrice/meta', 'CustomerUnitPrice\CustomerUnitPriceController@getMeta');
    Route::get('/CustomerUnitPrice/list', 'CustomerUnitPrice\CustomerUnitPriceController@getList');
    Route::get('/CustomerUnitPrice/get/{id}', 'CustomerUnitPrice\CustomerUnitPriceController@index');

    Route::post('/CustomerUnitPrice/post', 'CustomerUnitPrice\CustomerUnitPriceController@post');
    Route::post('/CustomerUnitPrice/save', 'CustomerUnitPrice\CustomerUnitPriceController@save');

    //納品先
    Route::get('/deliveryDestination/meta', 'DeliveryDestination\DeliveryDestinationController@getMeta');
    Route::get('/deliveryDestination/get/{id}', 'DeliveryDestination\DeliveryDestinationController@index');
    Route::post('/deliveryDestination/post', 'DeliveryDestination\DeliveryDestinationController@post');
    Route::post('/deliveryDestination/save', 'DeliveryDestination\DeliveryDestinationController@save');
    Route::get('/deliveryDestination/search/{word}', 'DeliveryDestination\DeliveryDestinationController@search');
    Route::get('/deliveryDestination/search/{account_id}/{word}', 'DeliveryDestination\DeliveryDestinationController@searchForAccount');
    Route::delete('/deliveryDestination/delete/{id}', 'DeliveryDestination\DeliveryDestinationController@delete');

    Route::get('/deliveryDestination/list', 'DeliveryDestination\DeliveryDestinationController@getList');
    Route::get('/deliveryDestination/search', 'DeliveryDestination\DeliveryDestinationController@searchDeliveryDestination');

    //商品
    Route::get('/product/meta', 'Product\ProductController@getMeta');
    Route::get('/product/get/{id}', 'Product\ProductController@index');
    Route::get('/product/list', 'Product\ProductController@getList');

    Route::post('/product/post', 'Product\ProductController@post');
    Route::post('/product/save', 'Product\ProductController@save');
    Route::get('/product/search/{word}', 'Product\ProductController@search');
    Route::get('/product/search', 'Product\ProductController@searchProduct');

    Route::get('/tax/default', 'Tax\TaxController@getDefaultTax');

    //社員
    Route::get('/employee/meta', 'Employee\EmployeeController@getMeta');
    Route::get('/employee/get/{id}', 'Employee\EmployeeController@index');
    Route::get('/employee/list', 'Employee\EmployeeController@getList');
    Route::post('/employee/post', 'Employee\EmployeeController@post');
    Route::post('/employee/save', 'Employee\EmployeeController@save');
    Route::get('/employee/search/{word}', 'Employee\EmployeeController@search');
    Route::get('/employee/search', 'Employee\EmployeeController@searchEmployee');
    Route::delete('/employee/delete/{id}', 'Employee\EmployeeController@delete');


    //部署
    Route::get('/department/meta', 'Employee\DepartmentController@getMeta');
    Route::get('/department/get/{id}', 'Employee\DepartmentController@index');
    Route::get('/department/search/{word}', 'Employee\DepartmentController@search');
    Route::get('/department/list', 'Employee\DepartmentController@getList');
    Route::post('/department/post', 'Employee\DepartmentController@post');
    Route::post('/department/save', 'Employee\DepartmentController@save');
    Route::delete('/department/delete/{id}', 'Employee\DepartmentController@delete');

    //出荷
    Route::get('/Shipment/meta', 'Shipment\ShipmentController@getMeta');
    Route::get('/Shipment/get/{id}', 'Shipment\ShipmentController@index');
    Route::get('/Shipment/list', 'Shipment\ShipmentController@getList');
    Route::post('/Shipment/post', 'Shipment\ShipmentController@post');
    Route::post('/Shipment/save', 'Shipment\ShipmentController@save');
    Route::get('/Shipment/searchCustomerUnitPrice/{word}', 'Shipment\ShipmentController@searchCustomerUnitPrice');
    Route::get('/Shipment/getItemPrice', 'Shipment\ShipmentController@getItemPrice');
    Route::get('/Shipment/search', 'Shipment\ShipmentController@searchShipment');
    Route::get('/Shipment/LineItemList/{id}', 'Shipment\ShipmentController@getLineItems');
    Route::get('/Shipment/account/list', 'Shipment\ShipmentController@getListWithAccount');
    Route::post('/Shipment/meal_form', 'Shipment\ShipmentController@getMealPdf');
    Route::delete('/Shipment/delete/{id}', 'Shipment\ShipmentController@delete');
    Route::get('/Shipment/history/{id}', 'Shipment\ShipmentController@getHistoryList');
    //請求
    Route::get('/invoice/meta', 'Invoice\InvoiceController@getMeta');
    Route::get('/invoice/get/{id}', 'Invoice\InvoiceController@index');
    Route::get('/invoice/list', 'Invoice\InvoiceController@getList');
    Route::post('/invoice/save', 'Invoice\InvoiceController@save');

    Route::post('/invoice/create', 'Invoice\InvoiceController@post');
    Route::get('/invoice/invoiceFormList/{id}', 'Invoice\InvoiceController@getInvoiceForms');
    Route::get('/invoice/shipmentList/{id}', 'Invoice\InvoiceController@getShipments');
    Route::get('/invoice/paymentList/{id}', 'Invoice\InvoiceController@getPaymentsByInvoiceId');

    Route::get('/invoice/payment/list', 'Invoice\InvoiceController@getPayments');
    Route::get('/invoice/payment/search', 'Invoice\InvoiceController@searchPayments');
    Route::get('/invoice/payment/meta', 'Invoice\InvoiceController@getPaymentMeta');
    Route::get('/invoice/payment/get/{id}', 'Invoice\InvoiceController@getPayment');
    // 2021/11/11 梅川追加分
    Route::get('/invoice/payment/group', 'Invoice\InvoiceController@getGroupList');
    // ここまで
    Route::post('/invoice/payment/save', 'Invoice\InvoiceController@savePayment');
    Route::post('/invoice/payment/post', 'Invoice\InvoiceController@postPayment');
    Route::delete('/invoice/payment/delete/{id}', 'Invoice\InvoiceController@deletePayment');
    Route::get('/invoice/search', 'Invoice\InvoiceController@searchInvoice');
    Route::get('/invoice/meal_form/{id}', 'Invoice\InvoiceController@getMealForm');
    Route::post('/invoice/createInvoiceForm', 'Invoice\InvoiceController@createInvoiceForm');
    Route::delete('/invoice/delete/{id}', 'Invoice\InvoiceController@delete');
    Route::get('/invoice/annual', 'Invoice\InvoiceController@getInvoicePayment');
    //帳票一覧
    //売上一覧表
    Route::post('/Shipment/listForm', 'Shipment\ShipmentController@getListForm');
    //入金一覧表
    Route::post('/invoice/payment/listForm', 'Invoice\InvoiceController@getPaymentListForm');
    //請求一覧表
    Route::post('/invoice/listForm', 'Invoice\InvoiceController@getListForm');
    //領収書
    Route::post('/invoice/receiptForm', 'Invoice\InvoiceController@getReceiptForm');

    Route::post('/invoice/bulkOutputInvoiceForm', 'Invoice\InvoiceController@bulkOutputInvoiceForm');
    Route::post('/invoice/bulkOutputReceptForm', 'Invoice\InvoiceController@bulkOutputReceptForm');
    Route::get('takeout/list', 'Takeout\TakeoutController@getList');
    Route::get('takeout/search', 'Takeout\TakeoutController@getList');
    Route::get('takeout/get/{id}', 'Takeout\TakeoutController@getDetail');
    Route::post('takeout/post', 'Takeout\TakeoutController@create');
    Route::post('takeout/save', 'Takeout\TakeoutController@update');
    Route::get('takeout/getSummary', 'Takeout\TakeoutController@getSummary');
    Route::get('takeout/entered/{user_id}', 'Takeout\TakeoutController@exists');
});