import axios from "axios";
const state = {
    user: null,
    header: null
};

const getters = {
    check: state => !!state.header,
    username: state => (state.user ? state.user.name : ""),
    header: state => state.header,
    user: state => state.user
};

const mutations = {
    setUser(state, user) {
        state.user = user;
    },
    setHeader(state, header) {
        state.header = header;
    }
};

const actions = {
    async register(context, data) {
        const response = await axios.post(this.apiUrl + "register", data);
        context.commit("setUser", response.data);
    },
    async login(context, data) {
        const response = await axios.post("/api/login", data);
        if (response.status == 200) {
            context.commit("setHeader", response.headers.authorization);
            document.cookie = "header=" + response.headers.authorization;
        }
        return response;
    },
    async logout(context) {
        await axios.post("/api/logout");
        context.commit("setUser", null);
        context.commit("setHeader", null);
        document.cookie = "header=";
    },
    async currentUser(context) {
        const response = await axios.get("/api/user", {
            headers: { Authorization: context.getters.header }
        });
        const user = response.data || null;
        context.commit("setUser", user);
        if (user == null) {
            context.commit("setHeader", null);
            document.cookie = "header=";
        }
    },
    reLogin(context) {
        var cookies = document.cookie;
        var cookiesArray = cookies.split(";");
        var header = null;
        for (var c of cookiesArray) {
            var cArray = c.split("=");
            if (cArray[0].indexOf("header") != -1) {
                header = cArray[1]
                break;
            }
        }
        context.commit("setHeader", header);

    }
};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
};
