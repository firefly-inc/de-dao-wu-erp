import Vue from "vue";
import App from "./App.vue";
import router from "./Routes";
import store from "./store/index";
import vuetify from "./plugins/vuetify";
import "./bootstrap";
import gv from "./mixins/env";
import input from "./mixins/input";
import validation from "./mixins/validation";
import uitil from "./mixins/uitil";

import axios from "axios";
import VueAxiosPlugin from "./mixins/vue-axios";

Vue.mixin(gv); // mixinに登録
Vue.mixin(input);
Vue.mixin(validation);
Vue.mixin(uitil);

const createApp = async () => {
    store.dispatch("auth/reLogin");
    await store.dispatch("auth/currentUser");

    Vue.directive('init', {
        bind: function (el, binding, vnode) {
            vnode.context[binding.arg] = binding.value
        }
    });

    Vue.use(VueAxiosPlugin, { axios: axios });
    Vue.prototype.$axios = axios

    new Vue({
        vuetify,
        router,
        render: h => h(App),
        store,
    }).$mount("#app");
};

createApp();
