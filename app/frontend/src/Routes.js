import Vue from "vue";
import Router from "vue-router";
import store from "./store";

//import Layout from "@/components/Layout/Layout";
import Login from "@/pages/Login/Login";
//商品
import PoductList from "@/pages/Product/productList";
import ProductDetail from "@/pages/Product/productDetail";
import ProductEdit from "@/pages/Product/productEdit";
import ProductCreate from "@/pages/Product/productCreate";
//得意先
import AccountCreate from "@/pages/Account/accountCreate";
import AccountList from "@/pages/Account/accountList";
import AccountDetail from "@/pages/Account/accountDetail";
import AccountEdit from "@/pages/Account/accountEdit";
import CustomerUnitPriceEdit from "@/pages/CustomerUnitPrice/CustomerUnitPriceEdit";
import CustomerUnitPriceDetail from "@/pages/CustomerUnitPrice/CustomerUnitPriceDetail";

import deliveryDestinationDetail from "@/pages/deliveryDestination/deliveryDestinationDetail";
import deliveryDestinationEdit from "@/pages/deliveryDestination/deliveryDestinationEdit";
import deliveryDestinationList from "@/pages/deliveryDestination/deliveryDestinationList";
//部署
import DepartmentCreate from "@/pages/Department/departmentCreate";
import DepartmentList from "@/pages/Department/departmentList";
import DepartmentDetail from "@/pages/Department/departmentDetail";
import DepartmentEdit from "@/pages/Department/departmentEdit";
//ユーザ
import EmployeeCreate from "@/pages/Employee/employeeCreate";
import EmployeeList from "@/pages/Employee/employeeList";
import EmployeeDetail from "@/pages/Employee/employeeDetail";
import EmployeeEdit from "@/pages/Employee/employeeEdit";
//出荷
import ShipmentCreate from "@/pages/Shipment/shipmentCreate";
import shipmentDetail from "@/pages/Shipment/shipmentDetail";
import shipmentEdit from "@/pages/Shipment/shipmentEdit";
import shipmentList from "@/pages/Shipment/shipmentList";

//請求処理
import salesConfirmation from "@/pages/salesConfirmation/salesConfirmation";
//import payment from "@/pages/Invoice/payment";
import invoice from "@/pages/Invoice/invoice";
import invoiceList from "@/pages/Invoice/invoiceList";
import invoiceDetail from "@/pages/Invoice/invoiceDetail";
import invoiceEdit from "@/pages/Invoice/invoiceEdit";
import AnnualBillingAmountList from "@/pages/Invoice/AnnualBillingAmountList";

import paymentEdit from "@/pages/Payment/paymentEdit";
import paymentDetail from "@/pages/Payment/paymentDetail";
import paymentList from "@/pages/Payment/paymentList";

import takeOutList from "@/pages/TakeOut/takeOutList";
import takeOutCreate from "@/pages/TakeOut/takeOutCreate";
import takeOutEdit from "@/pages/TakeOut/takeOutEdit";
import takeOutDetail from "@/pages/TakeOut/takeOutDetail";
import takeOutSummary from "@/pages/TakeOut/takeOutSumary";

import Top from "@/pages/top";

//import store from "./store";
Vue.use(Router);

export default new Router({
    mode: "history",
    routes: [
        {
            path: "/",
            name: "トップページ",
            component: Top,
            redirect: "/shipmentList",
            children: [
                //商品
                {
                    path: "/productList",
                    name: "商品/商品一覧",
                    component: PoductList
                },
                {
                    path: "/productdetail",
                    name: "商品/商品詳細",
                    component: ProductDetail
                },
                {
                    path: "/productedit",
                    name: "商品/商品編集",
                    component: ProductEdit
                },
                {
                    path: "/productcreate",
                    name: "商品/商品登録",
                    component: ProductCreate
                },
                //得意先
                {
                    path: "/accountList",
                    name: "得意先/得意先一覧",
                    component: AccountList
                },
                {
                    path: "/accountdetail",
                    name: "得意先/得意先詳細",
                    component: AccountDetail
                },
                {
                    path: "/accountedit",
                    name: "得意先/得意先編集",
                    component: AccountEdit
                },
                {
                    path: "/accountcreate",
                    name: "得意先/得意先作成",
                    component: AccountCreate
                },
                {
                    path: "/CustomerUnitPriceEdit",
                    name: "得意先/商品単価を編集",
                    component: CustomerUnitPriceEdit
                },
                {
                    path: "/CustomerUnitPriceDetail",
                    name: "得意先/商品単価",
                    component: CustomerUnitPriceDetail
                },
                {
                    path: "/deliveryDestinationList",
                    name: "得意先/納品先の一覧",
                    component: deliveryDestinationList
                },
                {
                    path: "/deliveryDestinationEdit",
                    name: "得意先/納品先を編集",
                    component: deliveryDestinationEdit
                },
                {
                    path: "/deliveryDestinationDetail",
                    name: "得意先/納品先",
                    component: deliveryDestinationDetail
                },
                //部署
                {
                    path: "/departmentList",
                    name: "部署/部署一覧",
                    component: DepartmentList
                },
                {
                    path: "/departmentDetail",
                    name: "部署/部署詳細",
                    component: DepartmentDetail
                },
                {
                    path: "/departmentEdit",
                    name: "部署/部署編集",
                    component: DepartmentEdit
                },
                {
                    path: "/departmentCreate",
                    name: "部署/部署作成",
                    component: DepartmentCreate
                },
                //社員
                {
                    path: "/employeeList",
                    name: "社員/社員一覧",
                    component: EmployeeList
                },
                {
                    path: "/employeeDetail",
                    name: "社員/社員詳細",
                    component: EmployeeDetail
                },
                {
                    path: "/employeeEdit",
                    name: "社員/社員編集",
                    component: EmployeeEdit
                },
                {
                    path: "/employeeCreate",
                    name: "社員/社員作成",
                    component: EmployeeCreate
                },
                //出荷
                {
                    path: "/shipmentCreate",
                    name: "出荷/新規出荷作成",
                    component: ShipmentCreate
                },
                {
                    path: "/shipmentDetail",
                    name: "出荷/出荷詳細",
                    component: shipmentDetail
                },
                {
                    path: "/shipmentEdit",
                    name: "出荷/出荷編集",
                    component: shipmentEdit
                },
                {
                    path: "/shipmentList",
                    name: "出荷/出荷一覧",
                    component: shipmentList
                },
                //請求
                {
                    path: "/salesConfirmation",
                    name: "請求/売上表",
                    component: salesConfirmation
                },
                {
                    path: "/invoiceList",
                    name: "請求/請求一覧",
                    component: invoiceList
                },
                {
                    path: "/invoiceCreate",
                    name: "請求/請求作成",
                    component: invoice
                },
                {
                    path: "/invoiceDetail",
                    name: "請求/請求詳細",
                    component: invoiceDetail
                },
                {
                    path: "/invoiceEdit",
                    name: "請求/請求を編集",
                    component: invoiceEdit
                },
                {
                    path: "/paymentEdit",
                    name: "請求/入金を編集",
                    component: paymentEdit
                },
                {
                    path: "/paymentDetail",
                    name: "請求/入金詳細",
                    component: paymentDetail
                },
                {
                    path: "/paymentList",
                    name: "請求/入金一覧",
                    component: paymentList
                },
                {
                    path: "/takeOutList",
                    name: "持ち出し一覧",
                    component: takeOutList
                },
                {
                    path: "/takeOutEdit",
                    name: "持ち出し数編集",
                    component: takeOutEdit
                },
                {
                    path: "/takeOutDetail",
                    name: "持ち出し詳細",
                    component: takeOutDetail
                },
                {
                    path: "/takeOutSummary",
                    name: "持ち出し数集計",
                    component: takeOutSummary
                },
                {
                    path: "/takeOutCreate",
                    name: "持ち出し入力",
                    component: takeOutCreate
                },
                {
                    path: "/annualBillingAmountList",
                    name: "年間請求金額確認",
                    component: AnnualBillingAmountList
                }
            ],
            beforeEnter(to, from, next) {
                if (store.getters["auth/check"]) {
                    next();
                } else {
                    next("/login");
                }
            }
        },
        {
            path: "/login",
            name: "Login",
            component: Login
        }
    ]
});
