import { mapActions } from "vuex";
export default {
    methods: {
        ...mapActions("snackbar", ["openSnackbar", "closeSnackbar"])
    }
};
