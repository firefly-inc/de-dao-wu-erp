import {
    extend,
    localize,
    ValidationObserver,
    ValidationProvider
} from "vee-validate";
const ja = {
    code: "ja",
    messages: {
        alpha: "{_field_}はアルファベットのみ使用できます",
        alpha_num: "{_field_}は英数字のみ使用できます",
        alpha_dash:
            "{_field_}は英数字とハイフン、アンダースコアのみ使用できます",
        alpha_spaces: "{_field_}はアルファベットと空白のみ使用できます",
        between: "{_field_}は{min}から{max}の間でなければなりません",
        confirmed: "{_field_}が一致しません",
        digits: "{_field_}は{length}桁の数字でなければなりません",
        dimensions:
            "{_field_}は幅{width}px、高さ{height}px以内でなければなりません",
        email: "{_field_}は有効なメールアドレスではありません",
        excluded: "{_field_}は不正な値です",
        ext: "{_field_}は有効なファイル形式ではありません",
        image: "{_field_}は有効な画像形式ではありません",
        is: "{_field_}が一致しません",
        length: "{_field_}は{length}文字でなければなりません",
        max_value: "{_field_}は{max}以下でなければなりません",
        max: "{_field_}は{length}文字以内にしてください",
        mimes: "{_field_}は有効なファイル形式ではありません",
        min_value: "{_field_}は{min}以上でなければなりません",
        min: "{_field_}は{length}文字以上でなければなりません",
        numeric: "{_field_}は数字のみ使用できます",
        oneOf: "{_field_}は有効な値ではありません",
        regex: "{_field_}のフォーマットが正しくありません",
        required: "{_field_}は必須項目です",
        required_if: "{_field_}は必須項目です",
        size: "{_field_}は{size}KB以内でなければなりません"
    }
};
localize("ja", ja);
import {
    required,
    alpha_num,
    alpha_dash,
    min,
    max,
    email,
    confirmed,
    digits,
    regex,
    numeric
} from "vee-validate/dist/rules";

extend("required", required);
extend("alpha_num", alpha_num);
extend("alpha_dash", alpha_dash);
extend("min", min);
extend("max", max);
extend("email", email);
extend("confirmed", confirmed);
extend("digits", digits);
extend("regex", regex);
extend("numeric", numeric);

export default {
    components: {
        ValidationProvider,
        ValidationObserver
    },
    data() {
        return {};
    }
};
