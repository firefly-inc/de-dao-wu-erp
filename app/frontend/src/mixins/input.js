import check from "./../components/input/check";
import date from "./../components/input/date";
import lockup from "./../components/input/lockupT";
import number from "./../components/input/number";
import radio from "./../components/input/radio";
import select from "./../components/input/select";
import text from "./../components/input/text";
import textarea from "./../components/input/Textarea";
import time from "./../components/input/time";
import password from "./../components/input/password";

export default {
    components: {
        I_radio: radio,
        I_number: number,
        I_select: select,
        I_check: check,
        I_lockup: lockup,
        I_date: date,
        I_text: text,
        I_textarea: textarea,
        I_time: time,
        I_password: password
    }
};
