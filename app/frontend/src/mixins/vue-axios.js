import store from "../store";

const VueAxiosPlugin = {}
export default VueAxiosPlugin.install = function (Vue, { axios }) {
    const csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content')
    const header = store.getters["auth/header"];
    axios.defaults.headers.common = {
        "X-Requested-With": "XMLHttpRequest",
        "X-CSRF-Token": csrf_token,
        Authorization: header,
    }

    Vue.axios = axios
    Object.defineProperties(Vue.prototype, {
        axios: {
            get() {
                return axios
            }
        }
    })
}