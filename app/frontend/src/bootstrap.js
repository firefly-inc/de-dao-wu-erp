window._ = require("lodash");
import { getCookieValue } from "./util";
/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    //window.Popper = require("popper.js").default;
    //window.$ = window.jQuery = require("jquery");
    //require("bootstrap");
} catch (e) {
    console.log(e);
}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require("axios");

// Ajaxリクエストであることを示すヘッダーを付与する

let token = document.head.querySelector('meta[name="csrf-token"]');

window.axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
window.axios.defaults.withCredentials = true;
window.axios.defaults.baseURL = "";
window.axios.defaults.headers.common["X-CSRF-TOKEN"] = token.content;

window.axios.interceptors.request.use(config => {
    // クッキーからトークンを取り出してヘッダーに添付する
    config.headers["X-XSRF-TOKEN"] = decodeURIComponent(
        getCookieValue("XSRF-TOKEN")
    );
    return config;
});
