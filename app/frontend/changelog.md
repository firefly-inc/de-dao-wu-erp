# Changelog

## [v.1.0.4]

### Updated
- Update dependencies
- Fix layout

## [v.1.0.3]

### Updated
- Change badge text in the sidebar
- Change alert text in the usermanager/ecommerce

## [v.1.0.2]

### Updated
- Update documentations
- Update dependencies

### Fixed
- Sidebar on documentations
- Clear code
 
## [v.1.0.1]

### Updated
- Update footer on login page
- Update some dependencies

## [v1.0.0]
Release Vue Material Admin Full
