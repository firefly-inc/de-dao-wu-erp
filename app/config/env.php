<?php
return [
    'WKHTMLTOIMAGE_BINARY' => env('WKHTMLTOIMAGE_BINARY'),
    'WKHTMLTOPDF_BINARY' => env('WKHTMLTOPDF_BINARY')
];