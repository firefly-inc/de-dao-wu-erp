<?php

namespace Database\Seeders;

use App\Models\Tax;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

class TaxTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tax::create([
            'percent' => 0.08,
            'start_date' => '2014-04-01',
            'default_flg' => 1
        ]);

        Tax::create([
            'percent' => 0.10,
            'start_date' => '2019-10-01',
        ]);
    }
}