<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('users')->truncate();
        User::create(
            [
                'last_name' => '梅川',
                'first_name' => '敦',
                'last_name_kana' => 'ウメカワ',
                'first_name_kana' => 'アツシ',
                'email' => 'a.umekawa@fireflyinc.io',
                'code' => '999999',
                'password' => Hash::make('pass'),
            ]
        );
    }
}