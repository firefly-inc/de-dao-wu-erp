<?php

namespace Database\Seeders;

use App\Models\Account;
use App\Models\CustomerUnitPrice;
use App\Models\Product;
use App\Models\Payment;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class PaymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Payments = Payment::all();
        foreach ($Payments as $Payment) {
            if($Payment->deposit_amount != null){
                $Payment->deposit_amount += 1;
                $Payment->save();
                $Payment->deposit_amount -= 1;
                $Payment->save();
            }else{
                $Payment->deposit_amount = 1;
                $Payment->save();
                $Payment->deposit_amount = null;
                $Payment->save();
            }
        }
    }
}
