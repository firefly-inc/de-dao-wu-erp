<?php

namespace Database\Seeders;

use App\Models\Account;
use App\Models\CustomerUnitPrice;
use App\Models\Product;
use App\Models\ShipmentLineItem;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Query\JoinClause;
class ShipmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $shipmentLineItemList = ShipmentLineItem::query()
        ->join('shipments', function (JoinClause $join) {
            $join->on('shipments.id', '=', 'shipment_line_items.shipment_id');
            $join->whereNull('shipments.subtotal');
            //$join->where('shipments.id','>','2021-09-1');
        })
        ->limit(300000)
        ->select('shipment_line_items.*') // 不要なカラムが含まれないように posts.* のみに絞り込む
        ->get();
        foreach ($shipmentLineItemList as $shipmentLineItem) {
            if($shipmentLineItem->quantity != null){
                $shipmentLineItem->quantity += 1;
                $shipmentLineItem->save();
                $shipmentLineItem->quantity -= 1;
                $shipmentLineItem->save();
            }else{
                $shipmentLineItem->quantity = 1;
                $shipmentLineItem->save();
                $shipmentLineItem->quantity = null;
                $shipmentLineItem->save();
            }
        }
    }
}