<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Account;
use App\Models\Invoice;
use Illuminate\Support\Facades\DB;
class InvoiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Accounts = Account::all();
        foreach ($Accounts as $account) {
            DB::table('invoices')->insert([
                'billing_date' => '2021-09-30',
                'account_id' => $account->id,
                'payment_date' => '2021-09-30',
                'deadline'=>'2021-09-30'
            ]);
        }
    }
}