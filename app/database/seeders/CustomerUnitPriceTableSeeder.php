<?php

namespace Database\Seeders;

use App\Models\Account;
use App\Models\CustomerUnitPrice;
use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CustomerUnitPriceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $productList = Product::where('deleted_at', null)->get();
        $accountList = Account::where('deleted_at', null)->get();

        $params = [];
        foreach ($productList as $product) {
            foreach ($accountList as $account) {
                $params[] = [
                    'product_id' => $product->id,
                    'account_id' => $account->id,
                    'unit_price' => $product->unit_price,
                    'start_date' => '1900-01-01',
                ];

                if (count($params) >= 1000) {
                    CustomerUnitPrice::insert($params);
                    $params = [];
                }
            }
        }

        CustomerUnitPrice::insert($params);
    }
}