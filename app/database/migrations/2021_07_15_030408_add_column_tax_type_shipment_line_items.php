<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTaxTypeShipmentLineItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shipment_line_items', function (Blueprint $table) {
            $table->string('TaxType')->nullable()->after('quantity'); //税区分
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipment_line_items', function (Blueprint $table) {
            $table->dropColumn('TaxType');
        });
    }
}