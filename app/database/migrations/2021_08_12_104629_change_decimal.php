<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeDecimal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shipment_line_items', function (Blueprint $table) {
            $table->decimal('tax', 8, 0)->nullable()->change();
        });
        Schema::table('customer_unit_prices', function (Blueprint $table) {
            $table->decimal('unit_price', 8, 0)->nullable()->change();
        });
        Schema::table('payments', function (Blueprint $table) {
            $table->decimal('deposit_amount', 8, 0)->nullable()->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
