<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();//名称
            $table->string('postal_code')->nullable();//郵便番号
            $table->string('adress')->nullable();//住所
            $table->string('phone_number')->nullable();//電話番号
            $table->string('kana')->nullable();//カナ
            $table->string('Abbreviation')->nullable();//略称
            $table->string('type')->nullable();//郵便番号
            $table->string('first_adress')->nullable();//住所1

            $table->string('second_adress')->nullable();//住所2
            $table->string('fax')->nullable();//fax


            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('accounts', function(Blueprint $table) {
            $table->bigInteger('billing_id')->nullable()->unsigned();
            $table->foreign('billing_id')
            ->references('id')->on('accounts')
            ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('accounts')->truncate();
        Schema::dropIfExists('accounts');
    }
}