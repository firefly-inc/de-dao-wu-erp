<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColTotalpayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounts', function (Blueprint $table) {
            $table->decimal('kurikoshi', 8, 0)->nullable();
        });
        Schema::table('invoices', function (Blueprint $table) {
            $table->decimal('deposited_amount', 8, 0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounts', function (Blueprint $table) {
            $table->dropColumn('kurikoshi');
        });
        Schema::table('invoices', function (Blueprint $table) {
            $table->dropColumn('deposited_amount');
        });
    }
}
