<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class DeliveryDestinationssTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_destinations', function (Blueprint $table) {

            $table->id();

            $table->string('name')->nullable();//名称
            $table->string('kana')->nullable();//カナ
            $table->string('postal_code')->nullable();//郵便番号
            $table->string('adress')->nullable();//住所
            $table->string('phone_number')->nullable();//電話番号
            $table->string('type')->nullable();//郵便番号
            $table->string('first_adress')->nullable();//住所1
            $table->string('second_adress')->nullable();//住所2
            $table->string('fax')->nullable();//fax

            $table->bigInteger('account_id')->unsigned();
            $table->foreign('account_id')
                ->references('id')->on('accounts')
                ->onDelete('cascade')->cascadeOnUpdate();


            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('delivery_destinations')->truncate();
        Schema::dropIfExists('delivery_destinations');
    }
}