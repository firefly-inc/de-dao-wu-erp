<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeDepositAmount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){

        Schema::table('payments', function (Blueprint $table) {
            $table->dropColumn('deposit_amount');

        });
        Schema::table('payments', function (Blueprint $table) {
            $table->decimal('deposit_amount', 8, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropColumn('deposit_amount');
        });
        Schema::table('payments', function (Blueprint $table) {
            $table->date('deposit_amount');
        });
    }
}
