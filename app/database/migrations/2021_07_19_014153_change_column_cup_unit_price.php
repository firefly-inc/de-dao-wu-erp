<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnCupUnitPrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_unit_prices', function (Blueprint $table) {
            $table->dropColumn('unit_urice');
            $table->decimal('unit_price', 8, 2)->nullable()->after('product_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_unit_prices', function (Blueprint $table) {
            $table->dropColumn('unit_price');
            $table->decimal('unit_urice', 8, 2)->nullable()->after('product_id');
        });
    }
}
