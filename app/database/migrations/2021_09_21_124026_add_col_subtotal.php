<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColSubtotal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->decimal('tax', 8, 0)->nullable();
            $table->decimal('subtotal', 8, 0)->nullable();
        });
        Schema::table('shipments', function (Blueprint $table) {
            $table->decimal('tax', 8, 0)->nullable();
            $table->decimal('subtotal', 8, 0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->dropColumn('tax');
            $table->dropColumn('subtotal');
        });
        Schema::table('shipments', function (Blueprint $table) {
            $table->dropColumn('tax');
            $table->dropColumn('subtotal');
        });
    }
}
