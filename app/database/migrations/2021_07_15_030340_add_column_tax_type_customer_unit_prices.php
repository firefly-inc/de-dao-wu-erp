<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTaxTypeCustomerUnitPrices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_unit_prices', function (Blueprint $table) {
            $table->string('TaxType')->nullable()->after('start_date'); //税区分
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_unit_prices', function (Blueprint $table) {
            $table->dropColumn('TaxType');
        });
    }
}