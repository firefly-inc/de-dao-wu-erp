<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShipmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipments', function (Blueprint $table) {
            $table->id();
            //件名
            $table->string('subject')->nullable();
            //得意先
            $table->bigInteger('account_id')->unsigned();
            $table->foreign('account_id')
                ->references('id')->on('accounts')
                ->onDelete('cascade');
            //配送先
            $table->bigInteger('delivery_destination_id')->unsigned()->nullable();
            $table->foreign('delivery_destination_id')
                ->references('id')->on('delivery_destinations')
                ->onDelete('cascade');
            //売上担当者
            $table->bigInteger('sales_employee_id')->unsigned()->nullable()->nullable();
            $table->foreign('sales_employee_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            //配送担当者
            $table->bigInteger('delivery_employee_id')->unsigned()->nullable();
            $table->foreign('delivery_employee_id')
                ->references('id')->on('users')
                ->onDelete('cascade');

            //備考
            $table->string('remarks')->nullable();

            $table->date('sales_date')->nullable();

            $table->timestamps();
        });
        Schema::create('shipment_line_items', function (Blueprint $table) {
            $table->id();
            //出荷
            $table->bigInteger('shipment_id')->unsigned();
            $table->foreign('shipment_id')
                ->references('id')->on('shipments')
                ->onDelete('cascade');
            //商品
            $table->bigInteger('product_id')->unsigned();
            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onDelete('cascade');
            //単価
                $table->decimal('unit_urice', 8, 2)->nullable();
            //数量
            $table->decimal('quantity', 8, 2)->nullable();

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipments');
    }
}