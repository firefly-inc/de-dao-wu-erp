<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();//名称
            $table->string('Class')->nullable();//区分
            $table->string('BigCategory')->nullable();//大カテゴリー
            $table->string('MiddleCategory')->nullable();//中カテゴリー
            $table->string('SmallCategory')->nullable();//小カテゴリー
            $table->string('TaxType')->nullable();//税区分
            $table->string('Inventory_unit')->nullable();//発注単位
            $table->string('purchase_unit_price')->nullable();//仕入れ単価
            $table->string('unit_price')->nullable();//単価
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('products')->truncate();
        Schema::dropIfExists('products');
    }
}