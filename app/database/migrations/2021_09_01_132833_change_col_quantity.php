<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColQuantity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shipment_line_items', function (Blueprint $table) {
            $table->decimal('quantity', 8, 0)->nullable()->change();
            $table->decimal('unit_price', 8, 0)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipment_line_items', function (Blueprint $table) {
            //
        });
    }
}
