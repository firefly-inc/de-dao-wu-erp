<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->bigInteger('staff_id')->unsigned()->nullable();
            $table->foreign('staff_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->string('remarks')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropForeign('customer_payments_staff_id_foreign');
            $table->dropColumn('staff_id');
            $table->dropColumn('remarks');
        });
    }
}