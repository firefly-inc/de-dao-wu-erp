<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            //入金済みフラグ
            $table->tinyinteger('deposited')->nullable();

            //請求日
            $table->date('billing_date')->nullable();

            //入金済みフラグ
            $table->date('payment_date')->nullable();

            //請求先
            $table->bigInteger('account_id')->unsigned()->nullable();
            $table->foreign('account_id')
                ->references('id')->on('accounts')
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('invoices')->truncate();
        Schema::dropIfExists('invoices');
    }
}