<?php

namespace App\Http\Service;

use App\Models\Account;
use App\Models\DeliveryDestination;

class DeliveryDestinationService
{
    public function createDefaultDeliveryDestination(Account $account)
    {
        $d = DeliveryDestination::create([
            'name' => $account->name,
            'kana' => $account->kana,
            'postal_code' => $account->postal_code,
            'adress' => $account->adress,
            'phone_number' => $account->phone_number,
            'type' => $account->type,
            'first_adress' => $account->first_adress,
            'second_adress' => $account->second_adress,
            'fax' => $account->fax,
            'account_id' => $account->id,
        ]);
        $account->id = $d->id;
        return $account->save();
    }

    /**
     * パラメータをもとに取引先を検索
     *
     * @param array $param
     * @return Collection
     */
    public function searchByCondition(array $param)
    {
        $query = DeliveryDestination::query();

        // 名称
        if (array_key_exists('name', $param) && !empty($param['name'])) {
            $query->where('name', 'LIKE', '%' . $param['name'] . '%');
        }

        // 名称
        if (array_key_exists('id', $param) && !empty($param['id'])) {
            $query->where('id', $param['id']);
        }

        return $query->get();
    }

    /**
     * IDをもとに情報を取得
     *
     * @param integer $id
     * @return DeliveryDestination
     */
    function findById(int $id)
    {
        return DeliveryDestination::find($id);
    }
}