<?php

namespace App\Http\Service;

use App\Models\Account;
use App\Models\CustomerUnitPrice;
use App\Models\Product;
use Illuminate\Database\Eloquent\Collection;

class ItemService
{
    /**
     * 取引先ごとの商品単価を登録
     *
     * @param integer $accountId
     * @return void
     */
    public function createAccountUnitPrice(int $accountId)
    {
        $itemList = Product::where('deleted_at', null)->get();
        foreach ($itemList as $item) {
            CustomerUnitPrice::create([
                'product_id' => $item->id,
                'account_id' => $accountId,
                'unit_price' => $item->unit_price,
                'start_date' => '1900-01-01',
                'TaxType' => $item->TaxType,
            ]);
        }
    }

    /**
     * 商品情報をもとに、すでに登録されている取引先ごとの単価を登録
     *
     * @param Product $product
     * @return void
     */
    public function createItemUnitPrice(Product $product)
    {
        $accountList = Account::where('deleted_at', null)->get();
        foreach ($accountList as $account) {
            CustomerUnitPrice::create([
                'product_id' => $product->id,
                'account_id' => $account->id,
                'unit_price' => $product->unit_price,
                'start_date' => '1900-01-01',
            ]);
        }
    }

    /**
     * パラメータをもとに商品を検索
     *
     * @param array $param
     * @return Collection
     */
    public function searchByCondition(array $param)
    {
        $query = Product::query();

        // 名称
        if (array_key_exists('name', $param) && !empty($param['name'])) {
            $query->where('name', 'LIKE', '%' . $param['name'] . '%');
        }

        // 種別
        if (array_key_exists('Class', $param) && !empty($param['Class'])) {
            $query->where('Class', 'LIKE', '%' . $param['Class'] . '%');
        }

        // 課税有無
        if (array_key_exists('taxation_flg', $param) && !empty($param['taxation_flg'])) {
            $query->where('taxation_flg', '%' . $param['taxation_flg'] . '%');
        }

        return $query->get();
    }
}