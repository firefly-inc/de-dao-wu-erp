<?php

namespace App\Http\Service;

use App\Models\Account;
use App\Models\Invoice;
use App\Models\Shipment;
use Carbon\Carbon;
use DateInterval;
use DateTime;
use DateTimeInterface;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InvoiceService
{
    public function create(Request $request)
    {
        DB::beginTransaction();
        try {
            // 得意先ごとにループ
            foreach ($request->get('shipments') as $account) {
                $invoice = Invoice::create([
                    'account_id' => $account['id'],
                    'billing_date' => $request->get('end_date'),
                    'payment_date' => $this->getPaymentDate($account['id'], $request->get('end_date')),
                    'deposited' => 0,
                    'starting_date' => $request->get('start_date'),
                    'deadline' => $request->get('end_date'),
                    'payments_class' => $account['cash_receipt'] == 1 ? '現金' : '',
                ]);
                // 出荷情報ごとにループ
                foreach ($account['shipments'] as $shipment) {
                    $data = Shipment::find($shipment['id']);
                    $data->invoice_id = $invoice->id;
                    $data->save();
                }
            }
        } catch (Exception $e) {
            DB::rollBack();
            \Log::info($e->getMessage() . ',' . $e->getFile() . ',' . $e->getCode() . ',' . $e->getLine());
            return false;
        }
        DB::commit();
        return true;
    }

    private function getPaymentDate(int $accountId, $billingDate)
    {
        $account = Account::find($accountId);
        $date = new DateTime($billingDate);
        $date = $date->format('Y-m');
        $date = $this->getDay($account->payment_day, $date);
        switch ($account->payment_month_type) {
            case '翌月':
                return $this->addMonth($date, 1);
            case '翌々月':
                return $this->addMonth($date, 2);
        }
    }

    private function getDay($payment_day, $month)
    {
        if ($payment_day == '末日') {
            return new DateTime('last day of ' . $month);
        } else {
            $dateStr = $month . '-' . sprintf('%02d', (int)$payment_day);
            return new DateTime($dateStr);
        }
    }

    /**
     * 請求情報検索
     *
     * @param array $param
     * @return Collection
     */
    public function searchByCondition(array $param)
    {
        return $this->searchByConditionQuery($param)->get();
    }

    /**
     * 請求情報検索用クエリを取得
     *
     * @param array $param
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function searchByConditionQuery(array $param)
    {
        $query = Invoice::query();

        // 入金予定日
        if (array_key_exists('start_payment_date', $param) && !empty($param['start_payment_date'])) {
            $query->where('payment_date', '>=', $param['start_payment_date']);
        }
        if (array_key_exists('end_payment_date', $param) && !empty($param['end_payment_date'])) {
            $query->where('payment_date', '<=', $param['end_payment_date']);
        }

        if (array_key_exists('start_billing_date', $param) && !empty($param['start_billing_date'])) {
            $query->where('billing_date', '>=', $param['start_billing_date']);
        }
        if (array_key_exists('end_billing_date', $param) && !empty($param['end_billing_date'])) {
            $query->where('billing_date', '<=', $param['end_billing_date']);
        }

        // 請求日
        if (array_key_exists('amount_min', $param) && !empty($param['amount_min'])) {
            $query->where('deposited_amount', '>=', $param['amount_min']);
        }
        if (array_key_exists('amount_max', $param) && !empty($param['amount_max'])) {
            $query->where('deposited_amount', '<=', $param['amount_max']);
        }

        //得意先id
        if (array_key_exists('account_id', $param) && !empty($param['account_id'])) {

            $acc_id = $param['account_id'];
            $query->Where('account_id',  $acc_id);
        }
        //得意先名
        if (array_key_exists('account_name', $param) && !empty($param['account_name'])) {

            $acc_name = $param['account_name'];
            $query->whereHas('account', function ($q) use ($acc_name) {
                $q->Where('name', 'like', '%' . $acc_name . '%');
            });
        }
        $query->orderBy('created_at', 'desc');
        return $query;
    }

    /**
     * 指定された月数分加算する
     *
     * @param DateTimeInterface $before 加算前のDateTimeオブジェクト
     * @param int 月数（指定ない場合は1ヶ月）
     * @return DateTime DateTimeオブジェクト
     */
    function addMonth(DateTimeInterface $before, int $month = 1)
    {
        $beforeMonth = $before->format("n");

        // 加算する
        $after       = $before->add(new DateInterval("P" . $month . "M"));
        $afterMonth  = $after->format("n");

        // 加算結果が期待値と異なる場合は、前月の最終日に修正する
        $tmpAfterMonth = $beforeMonth + $month;
        $expectAfterMonth = $tmpAfterMonth > 12 ? $tmpAfterMonth - 12 : $tmpAfterMonth;

        if ($expectAfterMonth != $afterMonth) {
            $after = $after->modify("last day of last month");
        }

        return $after;
    }

    public function getInvoicePdfData(array $param)
    {
        $query = DB::table('invoices')
            ->select(
                'invoices.id as invoice_id',
                'accounts.id as account_id',
                'accounts.kurikoshi as kurikoshi',
                'accounts.name as account_name',
                'invoices.subtotal as subtotal',
                'invoices.tax as tax',
                DB::raw('SUM(payments.deposit_amount) as amount'),
            )
            ->leftjoin('payments', 'invoices.id', '=', 'payments.invoice_id')
            ->join('accounts', 'invoices.account_id', '=', 'accounts.id')
            ->join('shipments', 'shipments.invoice_id', '=', 'invoices.id')
            ->join('shipment_line_items', function ($join) {
                $join->on('shipment_line_items.shipment_id', '=', 'shipments.id');
                $join->whereNull('shipment_line_items.deleted_at');
            });
        // 入金ずみ
        if (array_key_exists('isPaymented', $param) &&  $param['isPaymented'] != '0') {
            if ($param['isPaymented'] == '2') {
                $query->where('deposited', 1);
            } else {
                $query->where('deposited', 0);
            }
        }
        //得意先id
        if (array_key_exists('account_id', $param) && !empty($param['account_id'])) {

            $acc_id = $param['account_id'];
            $query->Where('account_id',  $acc_id);
        }
        $ed = '';
        $sd = '';
        // 請求日
        if (array_key_exists('start_billing_date', $param) && !empty($param['start_billing_date'])) {
            $sd = $param['start_billing_date'];
            $query->where('billing_date', '>=', $param['start_billing_date']);
        }
        if (array_key_exists('end_billing_date', $param) && !empty($param['end_billing_date'])) {
            $ed = $param['end_billing_date'];
            $query->where('billing_date', '<=', $param['end_billing_date']);
        }

        // 入金予定日
        if (array_key_exists('start_payment_date', $param) && !empty($param['start_payment_date'])) {
            $query->where('payment_date', '>=', $param['start_payment_date']);
        }
        if (array_key_exists('end_payment_date', $param) && !empty($param['end_payment_date'])) {
            $query->where('payment_date', '<=', $param['end_payment_date']);
        }

        $query->groupBy('invoices.id');
        $query->orderBy('account_id');
        $dataList = $query->get();
        \Log::alert(print_r($dataList, true));
        $response = [];
        $paymentSum = 0;
        $amountSum = 0;

        $thisTimeInvoiceSum = 0;
        $salesAmountTaxSum = 0;
        $thisTimeTaxSum = 0;
        $kurikoshiSum = 0;
        $salesAmountSum = 0;
        $pages = [[]];
        $page_detail_num = 12;
        foreach ($dataList as $data) {
            if (count($pages[count($pages) - 1]) == $page_detail_num) {
                array_push($pages, []);
            }
            $paymentSum += $data->amount;
            $amountSum += $data->amount;
            $thisTimeTaxSum += floor($data->tax);
            $salesAmountTaxSum += floor($data->subtotal + $data->tax);
            $thisTimeInvoiceSum +=  floor($data->subtotal + $data->tax);
            $kurikoshiSum += $data->kurikoshi - $data->subtotal;
            $salesAmountSum += $data->subtotal;
            array_push(
                $pages[count($pages) - 1],
                array(
                    'invoiceNo' => $data->account_id,
                    'name' => $data->account_name,
                    'lastTimeAmount' => '',
                    'kurikoshi' => $data->kurikoshi - $data->subtotal,
                    'payment' => $data->amount,
                    'salesAmount' => $data->subtotal,
                    'thisTimeTax' => floor($data->tax),
                    'salesAmountTax' => floor($data->subtotal + $data->tax),
                    'thisTimeInvoice' => floor($data->subtotal + $data->tax),
                )
            );
        }
        $response = [
            'pages' => $pages, 'start_date' => $sd, 'end_date' => $ed,
            'paymentSum' => $paymentSum,
            'amountSum' => $amountSum,
            'thisTimeTaxSum' => $thisTimeTaxSum,
            'salesAmountTaxSum' => $salesAmountTaxSum,
            'thisTimeInvoiceSum' => $thisTimeInvoiceSum,
            'salesAmountSum' => $salesAmountSum,
            'kurikoshiSum' => $kurikoshiSum,
        ];
        \Log::alert(print_r($response, true));
        return $response;
    }

    public function getInvoiceAndPayment(array $param)
    {

        $qStartDate = new Carbon($param['start_date'] . '-01');
        $qEndDate = (new Carbon($param['end_date'] . '-01'))->endOfMonth();
        // 得意先の取得
        $q = DB::table('accounts')->select('id');

        //得意先id
        if (array_key_exists('account_id', $param) && !empty($param['account_id'])) {

            $acc_id = $param['account_id'];
            $q->Where('id',  $acc_id);
        }
        //得意先名
        if (array_key_exists('account_name', $param) && !empty($param['account_name'])) {

            $acc_name = $param['account_name'];
            $q->Where('name', 'like', '%' . $acc_name . '%');
        }

        $q->whereExists(function ($q) use ($param) {
            $q->select(DB::raw(1))
                ->from('invoices')
                ->whereRaw('accounts.id = invoices.account_id');

            if (array_key_exists('start_date', $param) && !empty($param['start_date'])) {
                $q->where('billing_date', '>=',  new Carbon($param['start_date'] . '-01'));
            }
            if (array_key_exists('end_date', $param) && !empty($param['end_date'])) {
                $q->where('billing_date', '<=', (new Carbon($param['end_date'] . '-01'))->endOfMonth());
            }
        });

        // 得意先の検索結果数
        $accountSize = $q->count();

        $accountIdList = $q->limit($param['page_size'])
            ->offset(($param['page'] - 1) * $param['page_size'])
            ->get();

        $idList = array();
        foreach ($accountIdList as $id) {
            array_push($idList, $id->id);
        }

        // 請求情報の取得
        $invoiceList = $this->searchByConditionQuery($this->createParam($param))
            ->whereIn('account_id', $idList)
            ->get();
        $accountInvoiceList = array();
        foreach ($invoiceList as $invoice) {
            if (!array_key_exists($invoice->account_id, $accountInvoiceList)) {
                $accountInvoiceList += array($invoice->account_id => array());
            }
            $billingMonth = $this->getYearMonth($invoice->billing_date);
            if (array_key_exists($billingMonth, $accountInvoiceList[$invoice->account_id])) {
                array_push($accountInvoiceList[$invoice->account_id][$billingMonth], $invoice);
            } else {
                $accountInvoiceList[$invoice->account_id][$billingMonth] = array($invoice);
            }
        }

        $responseList['data'] = array();
        $dateFLg = true;
        $dateList = array();
        foreach ($accountInvoiceList as $accountId => $billingDateList) {
            $account = Account::find($accountId);
            $data = array();
            $data = array(
                'account_id' => $accountId,
                'account_name' => $account->name,
                'deadline' => $account->deadline,
                'invoices' => array()
            );
            $startDate = new Carbon($param['start_date'] . '-01');
            $endDate = (new Carbon($param['end_date'] . '-01'))->endOfMonth();
            for ($i = $startDate; $i < $endDate; $i->addMonth(1)) {
                if ($dateFLg) {
                    array_push($dateList, $this->getYearMonth($i));
                }
                if (array_key_exists($this->getYearMonth($i), $billingDateList)) {
                    $invoiceList = $billingDateList[$this->getYearMonth($i)];
                    $amount = 0;
                    $paymentTotal = 0;
                    $payments = array();
                    foreach ($invoiceList as $invoice) {
                        $amount += $invoice->subtotal + $invoice->tax;
                        $paymentTotal += $invoice->deposited_amount;
                        foreach ($invoice->Payments as $payment) {
                            $flg = true;
                            foreach ($payments as &$p) {
                                if (
                                    $p['type'] == $payment->payments_class
                                    && $p['payment_date'] ==  $payment->payment_date
                                ) {
                                    $p['amount'] += intval($payment->deposit_amount);
                                    $flg = false;
                                }
                            }
                            if ($flg) {
                                array_push($payments, array(
                                    'type' => $payment->payments_class,
                                    'amount' => intval($payment->deposit_amount),
                                    'payment_date' => $payment->payment_date
                                ));
                            }
                        }
                    }
                    array_push($data['invoices'], array(
                        'invoice_month' => $this->getYearMonth($i),
                        'amount' => $amount,
                        'payment_total' => $paymentTotal,
                        'payments' => $payments,
                    ));
                } else {
                    array_push($data['invoices'], array(
                        'invoice_month' => $this->getYearMonth($i),
                        'amount' => 0,
                        'payment_total' => 0,
                        'payments' => array()
                    ));
                }
            }
            array_push($responseList['data'], $data);
            $dateFLg = false;
        }
        $responseList['dateList'] = $dateList;
        $responseList['pagesize'] = ceil($accountSize / $param['page_size']);
        return $responseList;
    }

    public function createParam(array $param): array
    {
        $response = array();
        if (array_key_exists('start_date', $param)) {
            $response += ['start_billing_date' => new Carbon($param['start_date'] . '-01')];
        }
        if (array_key_exists('end_date', $param)) {
            $response += ['end_billing_date' => (new Carbon($param['end_date'] . '-01'))->endOfMonth()];
        }
        if (array_key_exists('account_name', $param)) {
            $response += ['account_name' => $param['account_name']];
        }
        if (array_key_exists('account_id', $param)) {
            $response += ['account_id' => $param['account_id']];
        }
        if (array_key_exists('page', $param)) {
            $response += ['page' => $param['page']];
        }
        return $response;
    }

    /**
     * 日付情報をもとに、Y-mの情報を取得します
     *
     * @param [type] $yearMonthDay
     * @return void
     */
    public function getYearMonth($yearMonthDay)
    {
        $date = new Carbon($yearMonthDay);
        return $date->year . '-' . sprintf('%02d', $date->month);
    }
}