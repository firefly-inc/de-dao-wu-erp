<?php

namespace App\Http\Service;

use App\Exceptions\DuplicateException;
use App\Models\Takeout;
use App\Models\TakeoutDetail;
use Doctrine\DBAL\Query\QueryBuilder;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use stdClass;

/**
 * 持ち出しに関するサービスクラス
 */
class TakeoutService
{
    protected $shipmentService;

    public function __construct(ShipmentService $shipmentService)
    {
        $this->shipmentService = $shipmentService;
    }

    /**
     * 検索条件を元に、持ち出し情報を取得
     *
     * @param array $param
     * @return QueryBuilder
     */
    public function getList(array $param)
    {
        $query = Takeout::query();

        if (array_key_exists('take_out_staff_id', $param) && !empty($param['take_out_staff_id'])) {
            $query->where('take_out_staff_id', $param['take_out_staff_id']);
        }

        if (array_key_exists('take_out_date', $param)&& !empty($param['take_out_date'])) {
            $query->where('take_out_date', $param['take_out_date']);
        }
        $query->whereNull('deleted_at');
        $query->orderBy('take_out_staff_id')->orderBy('take_out_date');

        return $query;
    }

    /**
     * 詳細情報を取得
     *
     * @param integer $id
     * @return Collection
     */
    public function getDetail(int $id)
    {
        $takeout = Takeout::find($id);
        $takeout->take_out_staff = $takeout->user;
        $takeout->take_out_staff_name = $takeout->user->last_name . " " . $takeout->user->first_name;
        foreach ($takeout->takeoutDetail as $detail) {
            $detail->product_name = $detail->product->name;
            /*
            array_push($detailList, array(
                $detail,
                'product' => $detail->product,
                'product_name' => $detail->product->name,
            ));
            */
        }


        return $takeout;
        /* array(
            $takeout,
            'take_out_staff' => $takeout->user,
            'take_out_detail' => $detailList
        );*/
    }


    /**
     * 登録
     *
     * @param array $param
     * @return void
     */
    public function create(array $param)
    {
        if ($this->getList($param)->exists()) {
            throw new DuplicateException('すでに、選択された日付の、持ち出し情報は登録されています。');
        }
        // パラメータにある商品IDが重複している場合、エラー
        if ($this->duplicateProductId($param)) {
            throw new DuplicateException('同じ商品が複数登録されています。');
        }
        DB::beginTransaction();
        try {
            $takeout = Takeout::create([
                'take_out_staff_id' => $param['take_out_staff_id'],
                'take_out_date' => $param['take_out_date'],
            ]);

            $this->createTakeoutDetail($param, $takeout->id);
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
        DB::commit();
    }

    /**
     * 更新処理
     *
     * @param array $param
     * @return void
     */
    public function update(array $param)
    {
        // パラメータにある商品IDが重複している場合、エラー
        if ($this->duplicateProductId($param)) {
            throw new DuplicateException();
        }
        DB::beginTransaction();
        try {
            $takeout = Takeout::find($param['id']);
            $takeout->take_out_staff_id = $param['take_out_staff_id'];
            $takeout->take_out_date = $param['take_out_date'];
            $takeout->save();

            $this->deleteTakeoutDetail($takeout->id);
            $this->createTakeoutDetail($param, $takeout->id);
        } catch (Exception $e) {

            DB::rollback();
            throw $e;
        }
        DB::commit();
    }

    /**
     * 商品IDを重複チェック
     *
     * @param array $param
     * @return void
     */
    public function duplicateProductId(array $param)
    {
        $idList = [];
        foreach ($param['takeout_detail'] as $item) {
            if (in_array($item['product_id'], $idList)) {
                return true;
            }
            if (!empty($item['product_id'])) {
                array_push($idList, $item['product_id']);
            }
        }
        return false;
    }

    /**
     * 持出詳細の登録
     *
     * @param array $param
     * @param integer $takeoutId
     * @return void
     */
    public function createTakeoutDetail(array $param, int $takeoutId)
    {
        foreach ($param['takeout_detail'] as $item) {
            if (!empty($item['product_id'])) {
                TakeoutDetail::create([
                    'take_out_id' => $takeoutId,
                    'product_id' => $item['product_id'],
                    'quantity' => $item['quantity'],
                ]);
            }
        }
    }

    /**
     * 持ち出しIDをもとに詳細を削除
     *
     * @param integer $takeoutId
     * @return void
     */
    public function deleteTakeoutDetail(int $takeoutId)
    {
        TakeoutDetail::where('take_out_id', $takeoutId)->delete();
    }

    /**
     * 出入集計情報を取得
     *
     * @param array $param
     * @return void
     */
    public function getSummary(array $param)
    {
        $takeoutList = $this->getList($param)->get();
        $shipmentList = $this->shipmentService->getShipmentList($param);
        $temp = [];
        foreach ($takeoutList as $takeout) {
            foreach ($takeout->takeoutDetail as $detail) {
                $data = new stdClass();
                $data->take_out_staff_id = $takeout->take_out_staff_id;
                $data->take_out_staff_name = $takeout->user->last_name . " " . $takeout->user->first_name;;
                $data->product_id = $detail->product->id;
                $data->product_name = $detail->product->name;
                $data->quantity = $detail->quantity;
                $data->shipment_quantity = 0;
                array_push($temp, $data);
            }
        }
        foreach ($shipmentList as $shipment) {
            foreach ($shipment->ShipmentLineItems as $detail) {
                $data = new stdClass();
                $data->take_out_staff_id = $shipment->delivery_employee_id;
                $data->take_out_staff_name = $shipment->DeliveryEmployee->last_name . " " . $shipment->DeliveryEmployee->first_name;;
                $data->product_id = $detail->product->id;
                $data->product_name = $detail->product->name;
                $data->quantity = 0;
                $data->shipment_quantity = $detail->quantity;
                array_push($temp, $data);
            }
        }

        $response = [];
        foreach ($temp as $t) {
            $flg = true;
            foreach ($response as $r) {
                if ($t->take_out_staff_id == $r->take_out_staff_id && $t->product_id == $r->product_id) {
                    $flg = false;
                    $r->quantity += $t->quantity;
                    $r->shipment_quantity += $t->shipment_quantity;
                    break;
                }
            }
            if ($flg) {
                array_push($response, $t);
            }
        }

        return $response;
    }
}