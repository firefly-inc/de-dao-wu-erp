<?php

namespace App\Http\Service;

use App\Models\Account;
use Illuminate\Database\Eloquent\Collection;

class AccountService
{
    /**
     * パラメータをもとに取引先を検索
     *
     * @param array $param
     * @return Collection
     */
    public function searchByCondition(array $param)
    {
        $query = Account::query();

        // 名称
        if (array_key_exists('name', $param) && !empty($param['name'])) {
            $query->where('name', 'LIKE', '%' . $param['name'] . '%');
        }

        // 名称
        if (array_key_exists('id', $param) && !empty($param['id'])) {
            $query->where('id', $param['id']);
        }

        return $query->get();
    }
}