<?php

namespace App\Http\Service;

use Illuminate\Database\Eloquent\Collection;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
class EmployeeService
{
    /**
     * パラメータをもとに取引先を検索
     *
     * @param array $param
     * @return Collection
     */
    public function searchByCondition(array $param)
    {
        $query = User::query();

        // 名称
        if (array_key_exists('name', $param) && !empty($param['name'])) {
            $word = $param['name'];
            $query->where(function ($q) use ($word) {
                $q->orWhere('last_name', 'like', '%' . $word . '%')->orWhere('first_name', 'like', '%' . $word . '%');
            });
        }

        //部署名
        if (array_key_exists('department_name', $param) && !empty($param['department_name'])) {
            $dd_name =$param['department_name'];
            $query->whereHas('department',function ($q) use ($dd_name) {
                $q->Where('name', 'like', '%' . $dd_name . '%');
            });
        }

        return $query->get();
    }

    public function update(array $param)
    {
        $user = User::find($param['id']);
        $user->email = $param['email'];
        $user->is_delivery_staff = $param['is_delivery_staff'];
        $user->last_name = $param['last_name'];
        $user->first_name = $param['first_name'];
        $user->last_name_kana = $param['last_name_kana'];
        $user->first_name_kana = $param['first_name_kana'];
        $user->code = $param['code'];
        $user->department_id = $param['department_id'];
        if (array_key_exists('password', $param) && !empty($param['password'])) {
            $user->password = Hash::make($param['password']);
        }
        return $user->save();
    }
}