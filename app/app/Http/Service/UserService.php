<?php

namespace App\Http\Service;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

/**
 * 社員に関するサービスクラス
 */
class UserService
{

    public function update(array $param)
    {
        $user = User::find($param['id']);
        $user->email = $param['email'];
        $user->last_name = $param['last_name'];
        $user->first_name = $param['first_name'];
        $user->last_name_kana = $param['last_name_kana'];
        $user->first_name_kana = $param['first_name_kana'];
        $user->code = $param['code'];
        $user->department_id = $param['department_id'];
        if (array_key_exists('password', $param) && !empty($param['password'])) {
            $user->password = Hash::make($param['password']);
        }
        return $user->save();
    }
}