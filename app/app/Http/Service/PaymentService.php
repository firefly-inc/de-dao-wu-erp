<?php

namespace App\Http\Service;

use App\Models\Payment;
use Illuminate\Support\Facades\DB;

/**
 * 入金に関するサービスクラス
 */
class PaymentService
{

    public function getPaymentList(array $param)
    {
        $query = DB::table('payments')
            ->select(
                'accounts.id as customerNo',
                'accounts.name as customerName',
                'departments.name as department',
                'users.last_name as last_name',
                'users.first_name as first_name',
                'payments.deposit_amount as deposit_amount',
                'payments.payment_date as paymentDate'
            )
            ->join('invoices', 'payments.invoice_id', '=', 'invoices.id')
            ->leftJoin('accounts', 'invoices.account_id', '=', 'accounts.id')
            ->leftJoin('users', 'accounts.staff_id', '=', 'users.id')
            ->leftJoin('departments', 'users.department_id', '=', 'departments.id');

        if (array_key_exists('start_payment_date', $param) && !empty($param['start_payment_date'])) {
            $query->where('payments.payment_date', '>=', $param['start_payment_date']);
        }

        if (array_key_exists('end_payment_date', $param) && !empty($param['end_payment_date'])) {
            $query->where('payments.payment_date', '<=', $param['end_payment_date']);
        }



        $paymentList = $query->get();

        $response = [];
        foreach ($paymentList as $payment) {
            $response[] = [
                'paymentDate' => $payment->paymentDate, // 入金日
                'no' => '', //$payment->payment_date, // 伝票番号
                'customerNo' => $payment->customerNo, // 得意先番号
                'customerName' => $payment->customerName, // 得意先名
                'subject' => '', // 件名
                'department' => $payment->department, // 部門
                'billingAddress' => $payment->customerName, // 請求先
                'depositPerson' => $payment->first_name . $payment->last_name, // 入金担当者
                'dealingsSection' => '', //$payment->payment_date, // 取引区分
                'PaymentAmount' => $payment->deposit_amount, // 入金金額
                'bank' => '', // 銀行
                'account' => '', // 口座
                'billsNo' => '', // 手形番号
                'billsDeadline' => '', // 手形期日
                'note' => '', // 摘要
                'slipTotal' => $payment->deposit_amount, // 伝票合計
            ];
        }
        return $response;
    }

    public function getPaymentListForm(array $param)
    {
        $query = DB::table('payments')
            ->select(
                'accounts.id as customerNo',
                'accounts.name as customerName',
                'departments.name as department',
                'users.last_name as last_name',
                'users.first_name as first_name',
                'payments.deposit_amount as deposit_amount',
                'payments.payment_date as paymentDate',
                'payments.payments_class as dealingsSection',
                'payments.remarks as note',
                'invoices.id as invoiceId'
            )
            ->join('invoices', 'payments.invoice_id', '=', 'invoices.id')
            ->leftJoin('accounts', 'invoices.account_id', '=', 'accounts.id')
            ->leftJoin('users', 'payments.staff_id', '=', 'users.id')
            ->leftJoin('departments', 'users.department_id', '=', 'departments.id');

        $ed = '';
        $sd = '';
        if (array_key_exists('start_payment_date', $param) && !empty($param['start_payment_date'])) {
            $query->where('payments.payment_date', '>=', $param['start_payment_date']);
            $sd = $param['start_payment_date'];
        }

        if (array_key_exists('end_payment_date', $param) && !empty($param['end_payment_date'])) {
            $query->where('payments.payment_date', '<=', $param['end_payment_date']);
            $ed = $param['end_payment_date'];
        }

        $query->whereNull('payments.deleted_at');

        $paymentList = $query->get();


        $pages = [[]];
        $page_detail_num = 6;
        $total_amount = 0;
        \Log::alert(print_r($paymentList,true));
        foreach ($paymentList as $payment) {
            if (count($pages[count($pages) - 1]) == $page_detail_num) {
                array_push($pages, []);
            }
            $total_amount += $payment->deposit_amount;
            $flg = true;
            foreach ($pages as &$dataList) {
                foreach ($dataList as &$data) {
                    if ($data['paymentDate'] == $payment->paymentDate && $data['invoiceId'] == $payment->invoiceId) {
                        $data['slipTotal'] += $payment->deposit_amount;
                        array_push($data['detail'], array(
                            'dealingsSection' => $payment->dealingsSection, // 取引区分 TODO
                            'PaymentAmount' => $payment->deposit_amount, // 入金金額 TODO
                            'bank' => '', // 銀行
                            'account' => '', // 口座
                            'billsNo' => '', // 手形番号
                            'billsDeadline' => '', // 手形期日
                            'note' => $payment->note, // 摘要
                        ));
                        $flg = false;
                    }
                }
            }
            if ($flg) {
                array_push(
                    $pages[count($pages) - 1],
                    array(
                        'invoiceId' => $payment->invoiceId, // 請求ID
                        'paymentDate' => $payment->paymentDate, // 入金日
                        'no' => '', //$payment->payment_date, // 伝票番号
                        'customerNo' => $payment->customerNo, // 得意先番号
                        'customerName' => $payment->customerName, // 得意先名
                        'subject' => '', // 件名
                        'department' => $payment->department, // 部門
                        'billingAddress' => $payment->customerName, // 請求先
                        'depositPerson' =>  $payment->last_name . ' ' . $payment->first_name, // 入金担当者
                        'slipTotal' => $payment->deposit_amount, // 伝票合計 TODO
                        'detail' => array(array(
                            'note' => $payment->note, // 摘要
                            'billsDeadline' => '', // 手形期日
                            'billsNo' => '', // 手形番号
                            'account' => '', // 口座
                            'dealingsSection' => $payment->dealingsSection, // 取引区分 TODO
                            'PaymentAmount' => $payment->deposit_amount, // 入金金額 TODO
                            'bank' => '', // 銀行
                        ))
                    )
                );
            }
        }
        \Log::alert(print_r($pages,true));

        $response = ['pages' => $pages, 'total_amount' => $total_amount, 'start_date' => $sd, 'end_date' => $ed];
        return $response;
    }

    /**
     * 請求情報検索
     *
     * @param array $param
     * @return Collection
     */
    public function searchByCondition(array $param)
    {
        $query = Payment::query();
        // 入金予定日
        if (array_key_exists('start_payment_date', $param) && !empty($param['start_payment_date'])) {
            $query->where('payment_date', '>=', $param['start_payment_date']);
        }
        if (array_key_exists('end_payment_date', $param) && !empty($param['end_payment_date'])) {
            $query->where('payment_date', '<=', $param['end_payment_date']);
        }
        //得意先情報
        if ((array_key_exists('account_id', $param) || array_key_exists('account_name', $param)) && (!empty($param['account_id']) || !empty($param['account_name']))) {
            $query->whereExists(function ($q) use ($param) {
                $q->select(DB::raw(1))
                    ->from('invoices')
                    ->whereRaw('payments.invoice_id = invoices.id')
                    ->whereExists(function ($q) use ($param) {
                        $q->select(DB::raw(1))
                            ->from('accounts')
                            ->whereRaw('invoices.account_id = accounts.id');
                        if (!empty($param['account_id'])) {
                            $q->where('accounts.id', $param['account_id']);
                        }
                        if (!empty($param['account_name'])) {
                            $q->where('accounts.name', 'LIKE', '%' . $param['account_name'] . '%');
                        }
                    });
            });
        }

        $query->whereNull('deleted_at');
        $query->orderBy('created_at', 'desc');
        \Log::info($query->toSql());

        return $query->get();
    }
}