<?php

namespace App\Http\Service;

use App\Models\Account;
use App\Models\Product;
use App\Models\Shipment;
use App\Models\ShipmentLineItem;
use DateTime;
use App\Models\UpdateHistory;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Auth;

/**
 * 出荷サービスクラス
 */
class ShipmentService
{
    /**
     * 検索条件をもとに出荷情報を検索
     *
     * @param array $param
     * @return Collection
     */
    public function searchByCondition(array $param)
    {
        $query = DB::table('shipments');

        // 売上計上日
        if (array_key_exists('start_sales_date', $param) && !empty($param['start_sales_date'])) {
            $query->where('shipments.sales_date', '>=', $param['start_sales_date']);
        }
        if (array_key_exists('end_sales_date', $param) && !empty($param['end_sales_date'])) {

            $query->where('shipments.sales_date', '<=', $param['end_sales_date']);
        }
        //配送担当者
        if (array_key_exists('delivery_employee_id', $param) && !empty($param['delivery_employee_id'])) {
            $query->where('shipments.delivery_employee_id', $param['delivery_employee_id']);
        }
        //売上担当者
        if (array_key_exists('sales_employee_id', $param) && !empty($param['sales_employee_id'])) {
            $query->where('shipments.sales_employee_id', $param['sales_employee_id']);
        }
        //得意先id
        if (array_key_exists('account_id', $param) && !empty($param['account_id'])) {
            $acc_id = $param['account_id'];
            $query->Where('shipments.account_id', $acc_id);
        }

        //得意先名
        if (array_key_exists('account_name', $param) && !empty($param['account_name'])) {
            $acc_name = $param['account_name'];
            $query->whereExists(function ($query) use ($acc_name) {
                $query->select(DB::raw(1))
                    ->from('accounts')
                    ->whereRaw('accounts.id = shipments.account_id')
                    ->Where('accounts.name', 'like', '%' . $acc_name . '%');
            });
        }
        //納入先名
        if (array_key_exists('delivery_destination_name', $param) && !empty($param['delivery_destination_name'])) {
            $dd_name = $param['delivery_destination_name'];
            $query->whereExists(function ($query) use ($dd_name) {
                $query->select(DB::raw(1))
                    ->from('delivery_destinations')
                    ->whereRaw('delivery_destinations.id = shipments.delivery_destination_id')
                    ->Where('delivery_destinations.name', 'like', '%' . $dd_name . '%');
            });
        }
        //納入先id
        if (array_key_exists('delivery_destination_id', $param) && !empty($param['delivery_destination_id'])) {

            $dd_id = $param['delivery_destination_id'];
            $query->Where('shipments.delivery_destination_id', $dd_id);
        }


        $query->join('users', 'shipments.delivery_employee_id', '=', 'users.id');
        $query->join('delivery_destinations', 'shipments.delivery_destination_id', '=', 'delivery_destinations.id');
        $query->select(
            'delivery_destinations.name as delivery_destination_name',
            'users.last_name as delivery_employee_name',
            'shipments.id',
            'shipments.sales_date',
            'shipments.subtotal',
            'shipments.sales_date',
            'shipments.created_at',
        );

        $query->orderBy('sales_date', 'desc');
        $query->orderBy('created_at', 'desc');
        $query->whereNull('shipments.deleted_at');
        \Log::debug($query->toSql());
        return $query->limit(2500)->get();
    }

    /**
     * 請求登録一覧用の出荷情報を取得
     *
     * @return array
     */
    public function getListWithAccount(array $param = array())
    {
        $query = DB::table('accounts')->select(
            'accounts.id as account_id',
            'accounts.name',
            'shipments.id as shipment_id',
            'shipments.sales_date as sales_date',
            'shipments.ship_date as ship_date',
            'shipments.cash_receipt as cash_receipt',
            'shipments.individual_invoice_flg as individual_invoice_flg',
            'shipments.delivery_destination_id as delivery_destination_id',
            'delivery_destinations.name as delivery_destination_name',
            DB::raw('sum(shipment_line_items.unit_price * shipment_line_items.quantity) unit_price')
        )->leftJoin('shipments', 'accounts.id', '=', 'shipments.account_id')
            ->leftJoin('shipment_line_items', 'shipments.id', '=', 'shipment_line_items.shipment_id')
            ->join('products', 'products.id', '=', 'shipment_line_items.product_id')
            ->join('delivery_destinations', 'delivery_destinations.id', '=', 'shipments.delivery_destination_id')
            ->where('invoice_id', null);

        // 締め日
        if (array_key_exists('deadline', $param) && !empty($param['deadline'])) {
            $query->where('deadline', $param['deadline']);
        }
        // 得意先担当者
        if (array_key_exists('staff_id', $param) && !empty($param['staff_id'])) {
            $query->where('staff_id', $param['staff_id']);
        }
        // 売り上げ計上日
        if (array_key_exists('start_sales_date', $param) && !empty($param['start_sales_date'])) {
            $query->where('sales_date', '>=', $param['start_sales_date']);
        }
        if (array_key_exists('end_sales_date', $param) && !empty($param['end_sales_date'])) {
            $query->where('sales_date', '<=', $param['end_sales_date']);
        }

        if (array_key_exists('account_name', $param) && !empty($param['account_name'])) {
            $query->where('accounts.name', $param['account_name']);
        }
        if (array_key_exists('account_id', $param) && !empty($param['account_id'])) {
            $query->where('accounts.id', $param['account_id']);
        }
        if (array_key_exists('delivery_destination_id', $param) && !empty($param['delivery_destination_id'])) {
            $query->whereExists(function ($q) use ($param) {
                $q->select(DB::raw(1))
                    ->from('delivery_destinations')
                    ->whereRaw('accounts.id = delivery_destinations.account_id')
                    ->where('id', $param['delivery_destination_id']);
            });
        }
        $query->whereNull('shipments.deleted_at');


        $query->groupBy('accounts.id')
            ->groupBy('products.id')
            ->groupBy('shipments.id')->orderBy('accounts.id');

        $result = $query->get();
        $response = [];
        // 検索結果
        foreach ($result as $row) {
            $shipmentFlg = true;
            if (count($response) > 0 && $row->individual_invoice_flg == 0 && $row->cash_receipt == 0) {
                // レスポンス
                foreach ($response as &$data) {
                    // すでに得意先がレスポンスに存在する場合
                    if ($data['id'] == $row->account_id && $data['individual_invoice_flg'] == 0 && $data['cash_receipt'] == 0) {
                        // レスポンスの出荷情報をループ
                        $shipmentFlg = true;
                        foreach ($data['shipments'] as &$shipment) {
                            // すでに登録されている出荷情報の場合
                            if ($row->shipment_id == $shipment['id']) {
                                $shipment['subtotal'] += $row->unit_price;
                                $shipmentFlg = false;
                            }
                        }
                        if ($shipmentFlg) {
                            array_push($data['shipments'], array(
                                'id' => $row->shipment_id,
                                'account_id' => $row->account_id,
                                'sales_date' => $row->sales_date,
                                'ship_date' => $row->ship_date,
                                'subtotal' => floor($row->unit_price),
                                'delivery_destination_id' => $row->delivery_destination_id,
                                'delivery_destination_name' => $row->delivery_destination_name,
                            ));
                            $data['length'] += 1;
                        }
                        $data['subtotal'] += $row->unit_price;
                        $shipmentFlg = false;
                    }
                }
            }
            if ($shipmentFlg) {
                array_push($response, array(
                    'id' => $row->account_id,
                    'name' => $row->name,
                    'length' => 1,
                    'subtotal' => $row->unit_price,
                    'cash_receipt' => $row->cash_receipt,
                    'individual_invoice_flg' => $row->individual_invoice_flg,
                    'shipments' => array(array(
                        'id' => $row->shipment_id,
                        'account_id' => $row->account_id,
                        'sales_date' => $row->sales_date,
                        'ship_date' => $row->ship_date,
                        'subtotal' => $row->unit_price,
                        'delivery_destination_id' => $row->delivery_destination_id,
                        'delivery_destination_name' => $row->delivery_destination_name,
                    )),
                ));
            }
        }
        return $response;
    }

    /**
     * 出荷情報の更新を行います
     *
     * @param array $param
     * @return Shipment
     */
    public function update(array $param)
    {
        DB::beginTransaction();
        try {
            $shipment = DB::table('shipments')->select(
                'id',
                'subject',
                'account_id',
                'delivery_destination_id',
                'sales_employee_id',
                'delivery_employee_id',
                'remarks',
                'sales_date',
                'ship_date',
                'status',
                'individual_invoice_flg',
                'cash_receipt'
            )->where('id', $param['id'])->get()->first();
            $shipment->shipment_line_items = DB::table('shipment_line_items')->select(
                'product_id',
                'unit_price',
                'tax',
                'quantity',
                'TaxType'
            )->where('shipment_id', $param['id'])->where('deleted_at', null)->get();
            // 履歴の登録
            $this->createShipmentHistory($param, $shipment);
            // 出荷情報の更新
            $this->updateShipment($param);
            // 出荷明細の更新
            $this->updateShipmentLineItem($param, $param['id']);
        } catch (Exception $e) {
            \Log::info($e->getMessage());
            DB::rollBack();
        }
        DB::commit();
        return $shipment;
    }

    /**
     * 出荷情報を更新
     *
     * @param array $param
     * @return Shipment
     */
    public function updateShipment($param)
    {
        $shipment = Shipment::find($param['id']);
        $shipment->subject = $param['subject'];
        $shipment->account_id = $param['account_id'];
        $shipment->delivery_destination_id = $param['delivery_destination_id'];
        $shipment->sales_employee_id = $param['sales_employee_id'];
        $shipment->delivery_employee_id = $param['delivery_employee_id'];
        $shipment->remarks = $param['remarks'];
        $shipment->sales_date = $param['sales_date'];
        $shipment->ship_date = $param['ship_date'];
        $shipment->status = $param['status'];
        $shipment->cash_receipt = $param['cash_receipt'];
        $shipment->individual_invoice_flg = $param['individual_invoice_flg'];
        $shipment->save();
        return $shipment;
    }

    /**
     * 出荷明細情報を更新
     *
     * @param array $param
     * @param integer $shipmentId
     * @return void
     */
    public function updateShipmentLineItem(array $param, int $shipmentId)
    {
        ShipmentLineItem::where('shipment_id', $shipmentId)->delete();
        foreach ($param['shipment_line_items'] as $data) {
            ShipmentLineItem::create([
                'shipment_id' => $shipmentId,
                'product_id' => $data['product_id'],
                'unit_price' => $data['unit_price'],
                'tax' => $data['tax'],
                'quantity' => $data['quantity'],
                'TaxType' => $data['TaxType'],
            ]);
        }
    }

    /**
     * 出荷情報の履歴情報を登録
     *
     * @param array $param
     * @param mixed $shipment
     * @return void
     */
    public function createShipmentHistory(array $param, $shipment)
    {
        $newShipment = $this->createShipmentHistoryParam($param);
        $oldShipment = $this->createShipmentHistoryParam($shipment);

        $newShipment = $this->createShipmentLineItemHistoryParam($param, $newShipment);
        $oldShipment = $this->createShipmentLineItemHistoryParam($shipment, $oldShipment);

        UpdateHistory::create([
            'update_table_name' => 'App\Models\Shipment',
            'update_table_id' => $param['id'],
            'old_value' => $oldShipment,
            'new_value' => $newShipment,
        ]);
    }

    /**
     * 出荷情報の履歴情報に必要な配列を作成
     *
     * @param mixed $data
     * @return array
     */
    private function createShipmentHistoryParam($data)
    {
        if (!is_array($data)) {
            $data = json_decode(json_encode($data), true);
        }
        $response = [];
        $shipmentColumnList = Schema::getColumnListing('shipments');
        foreach ($data as $index => $value) {
            if (in_array($index, $shipmentColumnList)) {
                $response += [$index => $value];
            }
        }
        $response += ['created_by' => Auth::guard('api')->user()->last_name . ' ' . Auth::guard('api')->user()->first_name];
        return $response;
    }

    /**
     * 出荷明細情報の履歴情報に必要な配列を作成
     *
     * @param mixed $data
     * @param array $shipmentArray
     * @return void
     */
    private function createShipmentLineItemHistoryParam($data, $shipmentArray)
    {
        if (!is_array($data)) {
            $data = json_decode(json_encode($data), true);
        }
        $shipmentLineItemColumnList = Schema::getColumnListing('shipment_line_items');
        $shipmentArray += array('shipment_line_items' => []);
        foreach ($data['shipment_line_items'] as $index => $value) {
            $shipmentArray['shipment_line_items'][$index] = [];
            foreach ($value as $i => $v) {
                if (in_array($i, $shipmentLineItemColumnList)) {
                    $shipmentArray['shipment_line_items'][$index] += [$i => $v];
                }
            }
            $shipmentArray['shipment_line_items'][$index] += ['created_by' => Auth::guard('api')->user()->last_name . ' ' . Auth::guard('api')->user()->first_name];
        }
        return $shipmentArray;
    }

    public function getMealPdfData($shipments, $start_date, $end_day, $iscreateInvoice = true)
    {
        // 出荷詳細を取得
        if ($iscreateInvoice) {
            foreach ($shipments as &$shipment) {
                $shipment += ['shipmentLineItems' => $this->getShipmentLineItemList($shipment['id'])];
            }
        }
        $account = Account::find($shipments[0]['account_id']);
        $data = [];
        $date_list = [];
        $date_view = [];
        $derivary_list = array();
        $item_list = array();
        $week_day_map = array(
            0 => '日',
            1 => '月',
            2 => '火',
            3 => '水',
            4 => '木',
            5 => '金',
            6 => '土'
        );
        $date = new DateTime($start_date);
        $start_date_view = $date->format('Y年m月d日');
        $end_date = new DateTime($end_day);
        $end_date_view =  $end_date->format('Y年m月d日');
        for ($date; $date <= $end_date; $date->modify('+1 days')) {
            array_push($date_list, $date->format('Y-m-d'));
            array_push($date_view, array('day' => $date->format('d'), 'day_of_week' => $week_day_map[$date->format('w')]));
        }
        foreach ($shipments as &$shipment) {
            if (!in_array($shipment['delivery_destination_id'], $derivary_list)) {
                $derivary_list += array($shipment['delivery_destination_id'] => array(
                    'id' => $shipment['delivery_destination_id'],
                    'name' => $shipment['delivery_destination_name'],
                ));
            }

            foreach ($shipment['shipmentLineItems'] as &$shipmentLineItem) {
                if (!in_array($shipmentLineItem['product_id'] . '_' . $shipmentLineItem['unit_price'], $item_list)) {
                    $item_list = $item_list + array($shipmentLineItem['product_id'] . '_' . $shipmentLineItem['unit_price'] =>   array(
                        'id' => $shipmentLineItem['product_id'],
                        'name' => $shipmentLineItem['product_name'],
                        'unit_price' => $shipmentLineItem['unit_price']
                    ));
                }
            }
        }
        $data = array();
        foreach ($derivary_list as &$d) {
            $items = array();
            foreach ($item_list as &$item) {
                $dates = array();
                foreach ($date_list as &$date) {
                    $dates[$date] = 0;
                }
                $items[$item['id'] . '_' . $item['unit_price']] = array(
                    'name' => $item['name'],
                    'unit_price' => $item['unit_price'],
                    'total' => 0,
                    'date_list' => $dates
                );
            }
            $data[$d['id']] = array(
                'name' => $d['name'] . '(' . $d['id'] . ')',
                'item_list' => $items
            );
        }
        \Log::alert($data);
        foreach ($shipments as &$shipment) {
            foreach ($shipment['shipmentLineItems'] as &$shipmentLineItem) {
                $data[$shipment['delivery_destination_id']]['item_list'][$shipmentLineItem['product_id'] . '_' . $shipmentLineItem['unit_price']]['date_list'][$shipment['sales_date']] += $shipmentLineItem['quantity'];
                $data[$shipment['delivery_destination_id']]['item_list'][$shipmentLineItem['product_id'] . '_' . $shipmentLineItem['unit_price']]['total'] += $shipmentLineItem['quantity'];
            }
        }

        return array(
            'detail' => $data,
            'dates' => $date_view,
            'end_date' => $end_date_view,
            'start_date' => $start_date_view,
            'account' => $account,
            'today' => date('Y年m月d日')
        );
    }

    /**
     * 出荷IDをもとに出荷詳細情報を取得します
     *
     * @param integer $shipmentId
     * @return void
     */
    public function getShipmentLineItemList($shipmentId)
    {

        return json_decode(json_encode(empty(Shipment::find($shipmentId)->ShipmentLineItems) ? array() : Shipment::find($shipmentId)->ShipmentLineItems), true);
    }

    public function getSalesPdfDate(array $param)
    {
        $query = DB::table('accounts')
            ->select(
                'delivery_destinations.name as name',
                'delivery_destinations.id as delivery_destinations_id',
                'accounts.id as id',
                'accounts.name as account_name',
                'shipment_line_items.quantity as quantity',
                'products.id as product_id',
                'products.name as product_name',
                DB::raw('SUM(shipment_line_items.quantity) as shipment_line_items_count')
            )
            ->join('shipments', 'accounts.id', '=', 'shipments.account_id')
            ->join('shipment_line_items', function ($join) {
                $join->on('shipment_line_items.shipment_id', '=', 'shipments.id');
                $join->whereNull('shipment_line_items.deleted_at');
            })
            ->join('products', 'products.id', '=', 'shipment_line_items.product_id')
            ->join('delivery_destinations', 'shipments.delivery_destination_id', '=', 'delivery_destinations.id');

        // 売上計上日
        $sd = '';
        $ed = '';
        if (array_key_exists('start_sales_date', $param) && !empty($param['start_sales_date'])) {
            $query->where('sales_date', '>=', $param['start_sales_date']);
            $sd = $param['start_sales_date'];
        }
        if (array_key_exists('end_sales_date', $param) && !empty($param['end_sales_date'])) {
            $query->where('sales_date', '<=', $param['end_sales_date']);
            $ed = $param['end_sales_date'];
        }
        $dn = '';
        //配送担当者
        if (array_key_exists('delivery_employee_id', $param) && !empty($param['delivery_employee_id'])) {
            $query->where('delivery_employee_id', $param['delivery_employee_id']);
            $dn = User::find($param['delivery_employee_id'])->name;
        }
        //売上担当者
        if (array_key_exists('sales_employee_id', $param) && !empty($param['sales_employee_id'])) {
            $query->where('sales_employee_id', $param['sales_employee_id']);
        }
        //得意先名
        if (array_key_exists('account_name', $param) && !empty($param['account_name'])) {
            $query->Where('accounts.name', 'like', '%' . $param['account_name'] . '%');
        }
        //配送先名
        if (array_key_exists('delivery_destination_name', $param) && !empty($param['delivery_destination_name'])) {
            $query->Where('delivery_destinations.name', 'like', '%' . $param['delivery_destination_name'] . '%');
        }
        //得意先id
        if (array_key_exists('account_id', $param) && !empty($param['account_id'])) {

            $acc_id = $param['account_id'];
            $query->Where('account_id', $acc_id);
        }

        //納入先id
        if (array_key_exists('delivery_destination_id', $param) && !empty($param['delivery_destination_id'])) {

            $dd_id = $param['delivery_destination_id'];
            $query->Where('delivery_destination_id', $dd_id);
        }
        $query->whereNull('shipment_line_items.deleted_at');
        $query->whereNull('shipments.deleted_at');
        $query->groupBy('delivery_destinations.id')->groupBy('products.id')->orderBy('delivery_destinations.id');

        $dataList = $query->get();
        $response = [[]];
        $page_detail_num = 12;
        $totals = [];
        $total = 0;
        foreach ($dataList as $data) {
            if (!array_key_exists(count($response) - 1, $totals)) {
                array_push($totals, 0);
            }
            $totals[count($response) - 1] += $data->shipment_line_items_count;
            $total += $data->shipment_line_items_count;
            array_push($response[count($response) - 1], array(
                'account_id' => $data->delivery_destinations_id,
                'account_name' => $data->name,
                'item_id' => $data->product_id,
                'item_name' => $data->product_name,
                'count' => $data->shipment_line_items_count,
            ));

            if (count($response[count($response) - 1]) == $page_detail_num) {
                array_push($response, []);
            }
        }
        array_push($response[count($response) - 1], array(
            'account_id' => '',
            'account_name' => '',
            'item_id' => '',
            'item_name' => '計',
            'count' => $total,
        ));
        $res = array('pages' => $response, 'user' => Auth::user(), 'totals' => $totals, 'start_date' => $sd, 'end_date' => $ed, 'driver_name' => $dn);
        return $res;
    }

    /**
     * 持ち出し差分データ作成用、出荷データ取得
     *
     * @param array $param
     * @return Collection
     */
    public function getShipmentList(array $param)
    {
        \Log::info($param);
        $query = Shipment::whereNull('deleted_at');
        if (array_key_exists('take_out_staff_id', $param) && !empty($param['take_out_staff_id'])) {
            $query->where('delivery_employee_id', $param['take_out_staff_id']);
        }

        if (array_key_exists('take_out_date', $param) && !empty($param['take_out_date'])) {
            $query->where('sales_date', $param['take_out_date']);
        }

        return $query->get();
    }

    public function getHistoryList(int $id)
    {
        $historyList = DB::table('update_histories')->select('old_value', 'new_value', 'created_at')->where('update_table_id', $id)->orderBy('id')->get();
        $responseList = array();
        $i = 1;
        foreach ($historyList as $history) {
            if ($i == 1) {
                $val = json_decode($history->old_value);
                foreach ($val->shipment_line_items as $item) {
                    array_push(
                        $responseList,
                        array(
                            'version' => $i,
                            'TaxType' => $item->TaxType,
                            'product_id' => $item->product_id,
                            'product_name' => Product::find($item->product_id)->name,
                            'quantity' => $item->quantity,
                            'tax' => $item->tax,
                            'unit_price' => $item->unit_price,
                            'create_date' => date_format(new Carbon($history->created_at), 'Y年m月d日  H時i分s秒'),
                            'created_by' => property_exists($item, 'created_by') ? $item->created_by : ''
                        ),
                    );
                }
                $i++;
            }
            $val = json_decode($history->new_value);
            foreach ($val->shipment_line_items as $item) {
                array_push($responseList, array(
                    'version' => $i,
                    'TaxType' => $item->TaxType,
                    'product_id' => $item->product_id,
                    'product_name' => Product::find($item->product_id)->name,
                    'quantity' => $item->quantity,
                    'tax' => $item->tax,
                    'unit_price' => $item->unit_price,
                    'create_date' => date_format(new Carbon($history->created_at), 'Y年m月d日  H時i分s秒'),
                    'created_by' => property_exists($item, 'created_by') ? $item->created_by : ''
                ),);
            }
            $i++;
        }
        return $responseList;
    }
}