<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AuthController extends Controller
{
    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    function login()
    {
        $credentials = request(['id', 'password']);

        // If email,password,email_verified_flg don't match
        if (!$token = auth('api')->attempt($credentials)) {
            $msg = "ログイン失敗";
            return response()->json(['status' => 401, 'message' => $msg]);
        }

        $msg = "ログイン成功";
        return response()->json(['status' => 200, 'message' => $msg])
            ->header('Authorization', 'bearer ' . $token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(Auth::guard('api')->user());
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    protected function username()
    {
        return 'id';
    }
}