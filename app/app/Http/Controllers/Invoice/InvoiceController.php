<?php

namespace App\Http\Controllers\Invoice;

use App\Http\Controllers\Controller;
use App\Http\Service\DeliveryDestinationService;
use App\Http\Service\InvoiceService;
use App\Http\Service\PaymentService;
use App\Http\Service\ShipmentService;
use App\Models\Account;
use Illuminate\Http\Request;
use App\Models\Invoice;
use App\Models\InvoiceForm;
use App\Models\Shipment;
use App\Models\Payment;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

/**
 * 請求に関するコントローラー
 */
class InvoiceController extends Controller
{
    private $invoiceService;
    private $shipmentService;
    private $paymentService;

    /**
     * コンストラクタ
     */
    public function __construct(
        InvoiceService $invoiceService,
        ShipmentService $shipmentService,
        PaymentService $paymentService
    ) {
        $this->invoiceService = $invoiceService;
        $this->shipmentService = $shipmentService;
        $this->paymentService = $paymentService;
    }

    public function index($id)
    {
        return array('meta' => Invoice::getFieldType(), 'data' => Invoice::find($id));
    }


    public function getMeta()
    {
        return array('meta' => Invoice::getFieldType());
    }

    public function getList()
    {
        return array('meta' => Invoice::getFieldType(), 'data' => Invoice::orderBy('created_at', 'desc')->limit(100)->get());
    }

    public function save(Request $request)
    {
        DB::beginTransaction();
        try {
            Invoice::find($request->id)->fill($request->all())->save();
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error('商品の登録に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
    }

    public function getPaymentMeta()
    {
        return array('meta' => Payment::getFieldType());
    }
    public function getPayment($id)
    {
        return array('meta' => Payment::getFieldType(), 'data' => Payment::find($id));
    }

    public function savePayment(Request $request)
    {
        DB::beginTransaction();
        try {
            Payment::find($request->id)->fill($request->all())->save();
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error('商品の登録に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
    }

    public function postPayment(Request $request)
    {
        DB::beginTransaction();
        try {
            Payment::create($request->all());
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error('入金の登録に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
    }
    /**
     * 削除
     *
     * @param mixed $id
     * @return bool
     */
    public function deletePayment($id)
    {
        DB::beginTransaction();
        try {
            Payment::where('id', $id)->delete();
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error('入金の削除に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
    }

    /**
     * 登録を行います。
     *
     * @param Request $request
     * @return void
     */
    public function post(Request $request)
    {
        set_time_limit(600);
        DB::beginTransaction();
        try {
            $this->invoiceService->create($request);
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error('請求情報の登録に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
    }

    /**
     * 検索
     *
     * @param Request $request
     * @return Collection
     */
    public function searchInvoice(Request $request)
    {
        return $this->invoiceService->searchByCondition($request->all());
    }

    /**
     * 請求書作成用情報
     *
     * @return void
     */
    public function getInvoiceForms(int $id)
    {
        return Invoice::find($id)->invoiceForm;
    }

    /**
     * 出荷情報一覧
     *
     * @return void
     */
    public function getShipments(int $id)
    {
        return Shipment::where('invoice_id', $id)->get();
    }

    /**
     * 請求に紐づく入金情報一覧
     *
     * @return void
     */
    public function getPaymentsByInvoiceId(int $id)
    {
        return Invoice::find($id)->Payments;
    }


    /**
     * 入金情報一覧
     *
     * @return void
     */
    public function getPayments()
    {
        return array('meta' => Payment::getFieldType(), 'data' => Payment::orderBy('created_at', 'desc')->get());
    }

    /**
     * 入金情報検索
     *
     * @return void
     */
    public function searchPayments(Request $request)
    {
        return $this->paymentService->searchByCondition($request->all());
    }

    /**
     * 請求書一括作成
     */
    public function bulkOutputInvoiceForm(Request $request)
    {
        DB::beginTransaction();
        $invoice_form_datas = [];
        try {
            foreach ($request->all() as $data) {
                array_push($invoice_form_datas, $this->InvoiceForm($data['id']));
            }
            $pdf = PDF::loadView('pdf.invoice.bulkOutputInvoiceForm', compact('invoice_form_datas'))
                ->setOption('page-size', 'A4')
                ->setOption('margin-right', 0)
                ->setOption('margin-top', 0)
                ->setOption('margin-bottom', 0)
                ->setOption('margin-left', 0);
            DB::commit();
            return $pdf->stream('sample.pdf');
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error('請求書の作成に失敗しました');
            \Log::error($e->getMessage());
            throw $e;
        }
    }
    /**
     * 請求書を作成
     */

    public function createInvoiceForm(Request $request)
    {
        DB::beginTransaction();
        try {
            $this->InvoiceForm($request->get('id'));
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error('請求書の作成に失敗しました');
            \Log::error($e->getMessage());
            throw $e;
        }
    }

    public function InvoiceForm($id)
    {

        $page_detail_num = 28; //1ページの表示量
        \Log::critical("message" . $id);
        $invoice = Invoice::find($id);
        $this->initPdfCreate($invoice->account_id);
        $light_tax = 0.00;
        $t_tax = 0.00;
        $outer_tax = 0.00;
        $inner_tax = 0.00;
        $DeliveryDestinations = [];
        foreach ($invoice->ShipmentsOrderByDeliveryDestination() as $shipment) {
            if (!array_key_exists($shipment['delivery_destination_id'], $DeliveryDestinations)) {
                $DeliveryDestinations[$shipment['delivery_destination_id']] = [];
            }
            array_push($DeliveryDestinations[$shipment['delivery_destination_id']], $shipment);
            foreach ($shipment->ShipmentLineItems as $ShipmentLineItem) {
                if ($ShipmentLineItem->product->tax == 0.08) {
                    $light_tax += $ShipmentLineItem->tax_amount;
                } else {
                    $t_tax += $ShipmentLineItem->tax_amount;
                }

                if (trim($ShipmentLineItem->TaxType) == '外税') {
                    $outer_tax += $ShipmentLineItem->tax_amount;
                } else {
                    $inner_tax += $ShipmentLineItem->tax_amount;
                }
            }
        }
        $datas = [];
        if (count($DeliveryDestinations) > 1) {
            $pages = [[]];
            array_push($pages[count($pages) - 1], array(
                'type' => 'shipmentDetail',
                'slip_date' => '',
                'slip_number' => '',
                'product_name' => '',
                'quantity' => '',
                'unit' => '',
                'unit_price' => '',
                'amount' => '',
                'remarks' => '',
            ));
            array_push($pages[count($pages) - 1], array(
                'type' => 'shipmentDetail',
                'slip_date' => '',
                'slip_number' => '',
                'product_name' => '',
                'quantity' => '',
                'unit' => '',
                'unit_price' => '',
                'amount' => '*御買上額*',
                'remarks' => '',
            ));
            $total_inner_tax = 0;
            $total_outer_tax = 0;
            $subtotal = 0;
            foreach ($DeliveryDestinations as $key => $value) {
                $s_subtotal = 0;
                $d_subtotal = 0;
                $outer_tax = 0.00;
                $inner_tax = 0.00;
                foreach ($value as $key2 => $shipment) {
                    $s_subtotal += $shipment->subtotal;



                    foreach ($shipment->ShipmentLineItems as $ShipmentLineItem) {
                        $d_subtotal += $ShipmentLineItem->quantity * $ShipmentLineItem->unit_price;
                        if ($ShipmentLineItem->product->tax == 0.08) {
                            $light_tax += $ShipmentLineItem->tax_amount;
                        } else {
                            $t_tax += $ShipmentLineItem->tax_amount;
                        }

                        if (trim($ShipmentLineItem->TaxType) == '外税') {
                            $outer_tax += $ShipmentLineItem->tax_amount;
                        } else {
                            $inner_tax += $ShipmentLineItem->tax_amount;
                        }
                    }
                }
                $d_subtotal = floor(floor($d_subtotal) - floor($inner_tax));
                $inner_tax = floor($inner_tax);
                $outer_tax = floor($outer_tax);
                $subtotal += $d_subtotal;
                $total_inner_tax += $inner_tax;
                $total_outer_tax += $outer_tax;
                array_push($pages[count($pages) - 1], array(
                    'type' => 'shipmentDetail',
                    'slip_date' => '',
                    'slip_number' => '',
                    'product_name' => $DeliveryDestinations[$key][0]->DeliveryDestination->name,
                    'quantity' => '',
                    'unit' => '',
                    'unit_price' => '',
                    'amount' => $d_subtotal,
                    'remarks' => '',
                ));
            }
            array_push($pages[count($pages) - 1], array(
                'type' => 'shipmentDetail',
                'slip_date' => '',
                'slip_number' => '',
                'product_name' => '',
                'quantity' => '',
                'unit' => '',
                'unit_price' => '計',
                'amount' => $subtotal,
                'remarks' => '',
            ));
            while (count($pages[count($pages) - 1]) < $page_detail_num) {
                array_push($pages[count($pages) - 1], array(
                    'type' => '',
                    'slip_date' => '',
                    'slip_number' => '',
                    'product_name' => '',
                    'quantity' => '',
                    'unit' => '',
                    'unit_price' => '',
                    'amount' => '',
                    'remarks' => '', //使わないけど一応
                ));
            }

            array_push($datas, array(
                'pages' => $pages,
                'account' => $invoice->account,
                'kurikoshi' => $invoice->account->kurikoshi,
                'name' => $invoice->account->name,
                'subtotal' => $subtotal,
                't_tax' => $t_tax,
                't_ltax' => $light_tax,
                'outer_tax' => $total_outer_tax,
                'inner_tax' => $total_inner_tax,
                'postal_code' => $invoice->account->postal_code,
                'first_adress' => $invoice->account->first_adress,
                'second_adress' => $invoice->account->second_adress,
                'code' =>  $invoice->account->id,
                'tax' => '',
                'invoice' => $invoice,
                'today' => date('Y年m月d日')
            ));
        }

        foreach ($DeliveryDestinations as $key => $value) {
            $pages = [[]];
            $d_tax = 0;
            $d_subtotal = 0;
            $d_name = '';
            $light_tax = 0.00;
            $tax = 0.00;
            $outer_tax = 0.00;
            $inner_tax = 0.00;
            $d_name = '';
            $first_adress = '';
            $second_adress = '';
            $postal_code = '';
            $code = '';

            foreach ($value as $key => $shipment) {
                $ship_tax = 0;
                if (!empty(trim($shipment->DeliveryDestination->name))) {
                }
                $d_name = trim($shipment->DeliveryDestination->name);
                if (!empty(trim($shipment->DeliveryDestination->first_adress))) {
                    $first_adress = trim($shipment->DeliveryDestination->first_adress);
                }
                if (!empty(trim($shipment->DeliveryDestination->second_adress))) {
                    $second_adress = trim($shipment->DeliveryDestination->second_adress);
                }
                if (!empty(trim($shipment->DeliveryDestination->postal_code))) {
                    $postal_code = trim($shipment->DeliveryDestination->postal_code);
                }
                if (!empty(trim($shipment->DeliveryDestination->id))) {
                    $code = trim($shipment->DeliveryDestination->id);
                }

                $quantity = 0;
                $first = true;
                foreach ($shipment->ShipmentLineItems as $ShipmentLineItem) {
                    $d_subtotal += $ShipmentLineItem->quantity * $ShipmentLineItem->unit_price;
                    if ($ShipmentLineItem->product->tax == 0.08) {
                        $light_tax += $ShipmentLineItem->tax_amount;
                    } else {
                        $tax += $ShipmentLineItem->tax_amount;
                    }
                    if (trim($ShipmentLineItem->TaxType) == '外税') {
                        $outer_tax += $ShipmentLineItem->tax_amount;
                    } else {
                        $inner_tax += $ShipmentLineItem->tax_amount;
                    }

                    $ship_tax += $ShipmentLineItem->quantity * $ShipmentLineItem->unit_price;
                    $quantity += $ShipmentLineItem->quantity;
                    if ($first) {
                        array_push(
                            $pages[count($pages) - 1],
                            array(
                                'type' => 'shipmentDetail',
                                'slip_date' => $shipment->sales_date,
                                'slip_number' => str_pad($shipment->id, 8, 0, STR_PAD_LEFT),
                                'product_name' => $ShipmentLineItem->product_name,
                                'quantity' => $ShipmentLineItem->quantity,
                                'unit' => '個',
                                'unit_price' => $ShipmentLineItem->unit_price,
                                'amount' => $ShipmentLineItem->unit_price * $ShipmentLineItem->quantity,
                                'remarks' => $ShipmentLineItem->TaxType . ($ShipmentLineItem->product->tax == 0.08 ? '*' : ''), //使わないけど一応
                            )
                        );
                        $first = false;
                    } else {
                        array_push(
                            $pages[count($pages) - 1],
                            array(
                                'type' => 'shipmentDetail',
                                'slip_date' => '',
                                'slip_number' => '',
                                'product_name' => $ShipmentLineItem->product_name,
                                'quantity' => $ShipmentLineItem->quantity,
                                'unit' => '個',
                                'unit_price' => $ShipmentLineItem->unit_price,
                                'amount' => $ShipmentLineItem->unit_price * $ShipmentLineItem->quantity,
                                'remarks' => $ShipmentLineItem->TaxType . ($ShipmentLineItem->product->tax == 0.08 ? '*' : ''), //使わないけど一応
                            )
                        );
                    }
                    if (count($pages[count($pages) - 1]) == $page_detail_num) {
                        array_push($pages, []);
                    }
                }
                array_push($pages[count($pages) - 1], array(
                    'type' => 'shipment',
                    'slip_date' => '',
                    'slip_number' => '',
                    'product_name' => '伝票合計',
                    'quantity' => $quantity,
                    'unit' => '個',
                    'unit_price' => '',
                    'amount' => $ship_tax,
                    'remarks' => '', //使わないけど一応
                ));
                if (count($pages[count($pages) - 1]) == $page_detail_num) {
                    array_push($pages, []);
                }
            }
            while (count($pages[count($pages) - 1]) < $page_detail_num) {
                array_push($pages[count($pages) - 1], array(
                    'type' => '',
                    'slip_date' => '',
                    'slip_number' => '',
                    'product_name' => '',
                    'quantity' => '',
                    'unit' => '',
                    'unit_price' => '',
                    'amount' => '',
                    'remarks' => '', //使わないけど一応
                ));
            }
            $d_subtotal = floor(floor($d_subtotal) - floor($inner_tax));
            $inner_tax = floor($inner_tax);
            $outer_tax =  floor($outer_tax);
            array_push($datas, array(
                'pages' => $pages,
                'kurikoshi' => $invoice->account->kurikoshi,
                'name' => count($DeliveryDestinations) > 1 ? $d_name : $invoice->account->name,
                'subtotal' => $d_subtotal,
                't_tax' => $tax,
                't_ltax' => $light_tax,
                'outer_tax' => $outer_tax,
                'inner_tax' => $inner_tax,
                'account' => $invoice->account,
                'postal_code' => count($DeliveryDestinations) > 1 ? $postal_code : $invoice->account->postal_code,
                'invoice' => $invoice,
                'today' => date('Y年m月d日'),
                'first_adress' => count($DeliveryDestinations) > 1 ?  $first_adress : $invoice->account->first_adress,
                'second_adress' => count($DeliveryDestinations) > 1 ?  $second_adress : $invoice->account->second_adress,
                'code' => count($DeliveryDestinations) > 1 ? $code : $invoice->account_id,
            ));
        }
        $filepath = $this->getPdfFilePath($invoice->account_id);
        $pdf = PDF::loadView('pdf.invoice.invoiceForm', compact('datas'))
            ->setOption('page-size', 'A4')
            ->setOption('margin-right', 0)
            ->setOption('margin-top', 0)
            ->setOption('margin-bottom', 0)
            ->setOption('margin-left', 0);
        $output = $pdf->output();
        file_put_contents($filepath, $output);
        InvoiceForm::create([
            'pdf_path' => $filepath,
            'invoice_id' => $id
        ]);
        return $datas;
    }

    /**
     * 食数確認表を取得
     *
     * @param integer $id
     * @return void
     */
    public function getMealForm(int $id)
    {
        $Invoice = Invoice::find($id);
        $data = $this->shipmentService->getMealPdfData(
            $Invoice->shipments,
            $Invoice->starting_date,
            $Invoice->deadline,
            false
        );
        //\Log::alert($data);
        try {
            $pdf = PDF::loadView('pdf.meal_form.meal_form', compact('data'))->setOption('page-width', '297')
                ->setOption('page-height', '210');
            return $pdf->stream('sample.pdf');
        } catch (Exception $e) {
            \Log::error('食数確認表の作成に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
    }

    /**
     * 食数確認表を取得
     *
     * @param integer $id
     * @return void
     */
    public function getReceiptForm(Request $request)
    {
        $Invoice = Invoice::find($request->get('id'));
        $data = array('invoice' => $Invoice);
        //\Log::alert($data);
        try {
            $pdf = PDF::loadView('pdf.receipt.receipt_form', compact('data'))
                ->setOption('page-size', 'A4')
                ->setOption('margin-right', 0)
                ->setOption('margin-top', 0)
                ->setOption('margin-bottom', 0)
                ->setOption('margin-left', 0);
            return $pdf->stream('sample.pdf');
        } catch (Exception $e) {
            \Log::error('領収書の作成に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
    }
    /**
     * 食数確認表を取得
     *
     * @param integer $id
     * @return void
     */
    public function bulkOutputReceptForm(Request $request)
    {
        $Invoices = [];
        foreach ($request->all() as $data) {
            array_push($Invoices, array('invoice' => Invoice::find($data['id'])));
        }
        //\Log::alert($data);
        try {
            $pdf = PDF::loadView('pdf.receipt.bulkOutputReceptForm', compact('Invoices'))
                ->setOption('page-size', 'A4')
                ->setOption('margin-right', 0)
                ->setOption('margin-top', 0)
                ->setOption('margin-bottom', 0)
                ->setOption('margin-left', 0);
            return $pdf->stream('sample.pdf');
        } catch (Exception $e) {
            \Log::error('領収書の作成に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
    }


    /**
     * 請求者作成時の初回処理
     *
     * @param integer $id
     * @return void
     */
    public function initPdfCreate($id)
    {
        $directory_path = public_path() . '/invoice/' . date('Y-m') . '/' . $id . '/';
        if (!File::exists($directory_path)) {
            File::makeDirectory($directory_path, $mode = 0777, true, true);
        }
    }

    /**
     * 請求書のパスを取得
     *
     * @param integer $id
     * @return string
     */
    public function getPdfFilePath($id)
    {
        return 'invoice/' . date('Y-m') . '/' . $id . '/請求書' . date('md_His') . '.pdf';
    }

    /**
     * 入金一覧表出力
     *
     * @param integer $id
     * @return string
     */
    public function getPaymentListForm(Request $request)
    {
        //TODO 入金一覧表出力処理
        $dataList = $this->paymentService->getPaymentListForm($request->all());
        $pdf = PDF::loadView('pdf.invoice.paymentListForm', compact('dataList'))->setOption('page-width', '297')
            ->setOption('page-height', '210')
            ->setOption('margin-right', 0)
            ->setOption('margin-top', 0)
            ->setOption('margin-bottom', 0)
            ->setOption('margin-left', 0);
        return $pdf->stream('sample.pdf');
    }

    /**
     * 請求一覧表出力
     *
     * @param integer $id
     * @return string
     */
    public function getListForm(Request $request)
    {
        //TODO 請求一覧表出力処理
        $dataList = $this->invoiceService->getInvoicePdfData($request->all());
        $pdf = PDF::loadView('pdf.invoice.invoiceListForm', compact('dataList'))->setOption('page-width', '297')
            ->setOption('page-height', '210');
        return $pdf->stream('sample.pdf');
    }

    /**
     * 削除
     *
     * @param [type] $id
     * @return void
     */
    public function delete($id)
    {
        DB::beginTransaction();
        try {
            Invoice::where('id', $id)->first()->delete();
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error('得意先の削除に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
    }

    /**
     * 請求IDと入金日をもとにグループ化した情報を取得
     *
     * @param Request $request
     * @return void
     */
    public function getGroupList(Request $request)
    {
        $paymentList = $this->paymentService->searchByCondition($request->all());
        $response = array();
        foreach ($paymentList as $payment) {
            $flg = true;
            foreach ($response as $res) {
                if ($res['invoice_id'] == $payment->invoice_id && $res['payment_date'] == $payment->payment_date) {
                    array_push($res['detail'], $payment);
                    $flg = false;
                }
            }
            if ($flg) {
                array_push($response, [
                    'invoice_id' => $payment->invoice_id,
                    'payment_date' => $payment->payment_date,
                    'detail' => [$payment]
                ]);
            }
        }

        return $response;
    }

    public function getInvoicePayment(Request $request)
    {
        return $this->invoiceService->getInvoiceAndPayment($request->all());
    }
}