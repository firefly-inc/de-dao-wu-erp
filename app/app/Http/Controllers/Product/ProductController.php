<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Tax\TaxController;
use App\Http\Service\ItemService;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Account;
use App\Models\Tax;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

/**
 * 商品に関するコントローラー
 */
class ProductController extends Controller
{
    private $itemService;

    public function __construct(ItemService $itemService)
    {
        $this->itemService = $itemService;
    }

    /**
     * IDをもとに情報を取得
     *
     * @param mixed $id
     * @return array
     */
    public function index($id)
    {
        return array('meta' => $this->getFieldType(), 'data' => Product::find($id));
    }

    /**
     * 商品フォームに必要なJSON情報を取得
     *
     * @return void
     */
    public function getMeta()
    {
        return array('meta' => $this->getFieldType());
    }

    /**
     * 商品フォームに必要なJSON情報を取得
     *
     * @return array
     */
    private function getFieldType()
    {
        // json取得
        $list = Product::getFieldType();
        // 税率を取得
        $taxList = Tax::where('start_date', '<=', date('Y-m-d'))->where('end_date', '>=', date('Y-m-d'))->get();
        // 税率の値を配列に
        $taxValueList = array();
        foreach ($taxList as $tax) {
            array_push($taxValueList, $tax->percent);
        }
        // jsonと結合
        $defaultTax =  Tax::where('default_flg', 1)->get()->first();
        array_splice(
            $list,
            4,
            0,
            [
                'tax' =>
                array(
                    'label' => '税率',
                    'type' => 'select',
                    'Required' => true,
                    'items' => $taxValueList,
                    'defaultValue' => $defaultTax->percent
                )
            ]
        );
        // keyがindex番号になるので、ループで税率のkeyを設定
        $response = [];
        foreach ($list as $key => $val) {
            if ($key === 0) {
                $response['tax'] = $list[$key];
            } else {
                $response[$key] = $list[$key];
            }
        }

        return $response;
    }

    /**
     * 一覧取得
     *
     * @return array
     */
    public function getList()
    {
        return array('meta' => Product::getFieldType(), 'data' => Product::all());
    }

    /**
     * 検索
     *
     * @param mixed $word
     * @return array
     */
    public function search($word)
    {
        return array('meta' => Product::getFieldType(), 'data' => Product::orWhere('name', 'like', '%' . $word . '%')->orWhere('id', 'like', '%' . $word . '%')->get());
    }

    /**
     * 更新
     *
     * @param Request $request
     * @param Product $product
     * @return bool
     */
    public function save(Request $request,  Product $product)
    {
        DB::beginTransaction();
        try {
            $product->find($request->id)->fill($request->all())->save();
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error('商品の登録に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
    }

    /**
     * 登録
     *
     * @param Request $request
     * @return Model
     */
    public function post(Request $request)
    {
        DB::beginTransaction();
        try {
            $product = Product::create($request->all());
            $this->itemService->createItemUnitPrice($product);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error('商品の登録に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
        return $product;
    }

    /**
     * 検索
     *
     * @param Request $request
     * @return Collection
     */
    public function searchProduct(Request $request)
    {
        return $this->itemService->searchByCondition($request->all());
    }
}