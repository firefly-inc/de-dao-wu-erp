<?php

namespace App\Http\Controllers\Account;

use App\Exceptions\APIException;
use App\Http\Controllers\Controller;
use App\Http\Service\AccountService;
use App\Http\Service\DeliveryDestinationService;
use App\Http\Service\ItemService;
use Illuminate\Http\Request;
use App\Models\Account;
use App\Models\CustomerUnitPrice;
use App\Models\DeliveryDestination;
use App\Models\Shipment;
use App\Models\Invoice;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

/**
 * 得意先に関するコントローラー
 */
class AccountController extends Controller
{
    private $itemService;

    private $deliveryDestinationService;

    private $accountService;

    public function __construct(ItemService $itemService, DeliveryDestinationService $deliveryDestinationService, AccountService $accountService)
    {
        $this->itemService = $itemService;
        $this->deliveryDestinationService = $deliveryDestinationService;
        $this->accountService = $accountService;
    }

    /**
     * 詳細情報取得
     *
     * @param mixed $id
     * @return array
     */
    public function index($id)
    {
        return array('meta' => Account::getFieldType(), 'data' => Account::find($id));
    }

    /**
     * 得意先のフォーム作成に必要なJSON情報を取得
     *
     * @return array
     */
    public function getMeta()
    {
        return array('meta' => Account::getFieldType());
    }

    /**
     * 得意先IDをもとに、納入先情報を取得
     *
     * @param mixed $id
     * @return Collection
     */
    public function getDeliveryDestinationsList($id)
    {
        return DeliveryDestination::where('account_id', '=', $id)->get();
    }

    /**
     * 得意先IDをもとに得意先単価情報を取得
     *
     * @param mixed $id
     * @return Collection
     */
    public function getPriceList($id)
    {
        return CustomerUnitPrice::where('account_id', '=', $id)->orderBy('product_id')->orderBy('start_date', 'desc')->get();
    }

    /**
     * 得意先IDをもとに出荷情報を取得
     *
     * @param mixed $id
     * @return Collection
     */
    public function getShipmentList($id)
    {
        $query = DB::table('shipments');


        $query->Where('shipments.account_id',$id);

        $query->join('users as delivery_user', 'shipments.delivery_employee_id', '=', 'delivery_user.id');
        $query->join('users as sales_user', 'shipments.sales_employee_id', '=', 'sales_user.id');
        $query->join('delivery_destinations', 'shipments.delivery_destination_id', '=', 'delivery_destinations.id');
        $query->select(
            'delivery_destinations.name as delivery_destination_name',
            'delivery_user.last_name as delivery_employee_name',
            'sales_user.last_name as sales_employee_name',
            'shipments.id',
            'shipments.sales_date',
            'shipments.subtotal',
            'shipments.sales_date',
            'shipments.created_at',
        );

        $query->orderBy('shipments.sales_date', 'desc');
        $query->orderBy('shipments.created_at', 'desc');
        $query->whereNull('shipments.deleted_at');
        return $query->limit(2500)->get();
    }

    /**
     * 得意先IDをもとに請求情報を取得
     *
     * @param mixed $id
     * @return Collection
     */
    public function getInvoiceList($id)
    {
        return Invoice::where('account_id', '=', $id)->orderBy('billing_date', 'desc')->limit(50)->get();
    }

    /**
     * 検索
     *
     * @param mixed $word
     * @return array
     */
    public function search($word)
    {
        return array('meta' => Account::getFieldType(), 'data' => Account::orWhere('name', 'like', '%' . $word . '%')->orWhere('id', 'like', '%' . $word . '%')->limit(30)->get());
    }

    /**
     * 得意先情報を取得
     *
     * @return array
     */
    public function getList()
    {
        return array('meta' => Account::getFieldType(), 'data' => Account::limit(100)
        ->get());
    }

    /**
     * 更新
     *
     * @param Request $request
     * @return bool
     */
    public function save(Request $request)
    {
        DB::beginTransaction();
        try {
            Account::find($request->id)->fill($request->all())->save();
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error('得意先の更新に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
    }

    /**
     * 登録
     *
     * @param Request $request
     * @return Account
     */
    public function post(Request $request)
    {
        DB::beginTransaction();
        try {
            $account = Account::create($request->all());
            $this->deliveryDestinationService->createDefaultDeliveryDestination($account);
            $this->itemService->createAccountUnitPrice($account->id);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error('得意先の登録に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
        return $account;
    }

    /**
     * 検索
     *
     * @param Request $request
     * @return Collection
     */
    public function searchAccount(Request $request)
    {
        return $this->accountService->searchByCondition($request->all());
    }

    /**
     * 削除
     *
     * @param [type] $id
     * @return void
     */
    public function delete($id)
    {
        DB::beginTransaction();
        try {
            Account::where('id', $id)->delete();
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error('得意先の削除に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
    }
}
