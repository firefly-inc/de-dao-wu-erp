<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Service\EmployeeService;
use App\Models\Account;
use App\Models\User;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

/**
 * 社員に関するコントローラー
 */
class EmployeeController extends Controller
{

    private $employeeService;
    public function __construct(EmployeeService $employeeService)
    {
        $this->employeeService = $employeeService;
    }



    /**
     * IDをもとに情報取得
     *
     * @param mixed $id
     * @return array
     */
    public function index($id)
    {
        return array('meta' => User::getFieldType(), 'data' => User::find($id));
    }

    /**
     * 社員フォームに必要なJSON情報を取得
     *
     * @return void
     */
    public function getMeta()
    {
        return array('meta' => User::getFieldType());
    }

    /**
     * 検索
     *
     * @param mixed $word
     * @return array
     */
    public function search($word)
    {
        return array('meta' => User::getFieldType(), 'data' => User::orWhere('last_name', 'like', '%' . $word . '%')->orWhere('id', 'like', '%' . $word . '%')->get());
    }

    /**
     * 全権取得
     *
     * @return array
     */
    public function getList()
    {
        return array('meta' => User::getFieldType(), 'data' => User::all());
    }

    /**
     * 更新
     *
     * @param Request $request
     * @return bool
     */
    public function save(Request $request)
    {
        DB::beginTransaction();
        try {
            $this->employeeService->update($request->all());
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error('社員の更新に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
    }

    /**
     * 登録
     *
     * @param Request $request
     * @return Model
     */
    public function post(Request $request)
    {
        DB::beginTransaction();
        try {
            $df = false;
            if(!empty($request['password'])){
                $request['password'] = Hash::make($request['password']);
            }else{
                $df = true;
                $request['password'] = 'dammy';
            }

            $user = User::create($request->all());
            if($df){
                $user->password = Hash::make($user->id);
                $user->save();
            }
            DB::commit();
            return $user;
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error('社員の登録に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
    }

    /**
     * 削除
     *
     * @param mixed $id
     * @return bool
     */
    public function delete($id)
    {
        DB::beginTransaction();
        try {
            User::where('id', $id)->delete();
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error('社員の削除に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
    }

    /**
     * 検索
     *
     * @param Request $request
     * @return Collection
     */
    public function searchEmployee(Request $request)
    {
        return $this->employeeService->searchByCondition($request->all());
    }
}