<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Department;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * 部署に関するコントローラー
 */
class DepartmentController extends Controller
{
    /**
     * IDをもとに情報取得
     *
     * @param mixed $id
     * @return array
     */
    public function index($id)
    {
        return array('meta' => Department::getFieldType(), 'data' => Department::find($id));
    }

    /**
     * 部署フォームに必要なJSONを取得
     *
     * @return array
     */
    public function getMeta()
    {
        return array('meta' => Department::getFieldType());
    }

    /**
     * IDをもとに情報取得
     *
     * @param mixed $id
     * @return array
     */
    public function getDeliveryDestinationsList($id)
    {
        return array('data' => Department::where('account_id', '=', $id)->get());
    }

    /**
     * 検索
     *
     * @param mixed $word
     * @return array
     */
    public function search($word)
    {
        return array('meta' => Department::getFieldType(), 'data' => Department::where('name', 'like', '%' . $word . '%')->get());
    }

    /**
     * 全権取得
     *
     * @return array
     */
    public function getList()
    {
        return array('meta' => Department::getFieldType(), 'data' => Department::all());
    }

    /**
     * 更新
     *
     * @param Request $request
     * @return bool
     */
    public function save(Request $request)
    {
        DB::beginTransaction();
        try {
            Department::where('id', $request->id)->update($request->all());
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error('部署の更新に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
    }

    /**
     * 登録
     *
     * @param Request $request
     * @return Model
     */
    public function post(Request $request)
    {
        DB::beginTransaction();
        try {
            $department = Department::create($request->all());
            DB::commit();
            return $department;
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error('部署の登録に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
    }

    /**
     * 削除
     *
     * @param mixed $id
     * @return bool
     */
    public function delete($id)
    {
        DB::beginTransaction();
        try {
            Department::where('id', $id)->delete();
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error('部署の削除に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
    }
}