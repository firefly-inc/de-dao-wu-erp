<?php

namespace App\Http\Controllers\Shipment;

use App\Http\Controllers\Controller;
use App\Http\Service\ShipmentService;
use Illuminate\Http\Request;
use App\Models\Account;
use App\Models\CustomerUnitPrice;
use App\Models\Product;
use App\Models\Shipment;
use App\Models\Invoice;
use App\Models\ShipmentLineItem;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Auth;

/**
 * 出荷に関するコントローラー
 */
class ShipmentController extends Controller
{
    private $shipmentService;

    public function __construct(ShipmentService $shipmentService)
    {
        $this->shipmentService = $shipmentService;
    }

    /**
     * IDをもとに情報取得
     *
     * @param mixed $id
     * @return array
     */
    public function index($id)
    {
        return array('meta' => Shipment::getFieldType(), 'data' => Shipment::find($id));
    }

    /**
     * 出荷IDをもとに出明細情報を取得
     *
     * @param int $id
     * @return array
     */
    public function getLineItems($id)
    {
        return array('data' => ShipmentLineItem::where('shipment_id', '=', $id)->get());
    }

    /**
     * 出荷フォームに必要なJSON情報を取得
     *
     * @return array
     */
    public function getMeta()
    {
        return array('meta' => Shipment::getFieldType());
    }

    /**
     * 出荷フォームで必要な得意先単価情報を検索
     *
     * @param [type] $word
     * @return array
     */
    public function searchCustomerUnitPrice($word)
    {
        $res = array('meta' => CustomerUnitPrice::getFieldType(), 'data' => Product::orWhere('id', 'LIKE', '%' . $word . '%')->orWhere('name', 'LIKE', '%' . $word . '%')->get());
        \Log::alert($res);
        return $res;
    }

    /**
     * 一覧取得
     *
     * @return array
     */
    public function getList()
    {
        \Log::debug("test2");
        return array('meta' => Shipment::getFieldType(), 'data' => Shipment::orderBy('created_at', 'desc')->limit(100)->get());
    }

    /**
     * 更新
     *
     * @param Request $request
     * @return bool
     */
    public function save(Request $request)
    {
        DB::beginTransaction();
        try {
            $this->shipmentService->update($request->all());
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error('出荷情報の更新に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
    }

    /**
     * 登録
     *
     * @param Request $request
     * @return Model
     */
    public function post(Request $request)
    {
        DB::beginTransaction();
        try {
            $shipment = Shipment::create(
                [
                    "subject" => $request['subject'],
                    "account_id" => $request['account_id'],
                    "delivery_destination_id" => $request['delivery_destination_id'],
                    "delivery_employee_id" => $request['delivery_employee_id'],
                    "sales_employee_id" => $request['sales_employee_id'],
                    "remarks" => $request['remarks'],
                    "sales_date" => $request['sales_date'],
                    "individual_invoice_flg" => $request['individual_invoice_flg'],
                    "cash_receipt" => $request['cash_receipt'],
                ]
            );
            foreach ($request['shipmentLineItem'] as $detail) {
                if ($detail['item'] != null) {
                    $product = Product::find($detail['item']);
                    ShipmentLineItem::create([
                        'shipment_id' => $shipment->id,
                        'product_id' => $detail['item'],
                        'unit_price' => $detail['unit_price'],
                        'quantity' => $detail['quantity'],
                        'TaxType' => $detail['TaxType'] == null ? '非課税' : $detail['TaxType'],
                        'tax' => $product->tax,
                    ]);
                }
            }
            DB::commit();
        } catch (Exception $e) {
            \Log::error("出荷情報の登録に失敗しました。");
            \Log::error($e->getMessage());
            DB::rollBack();
            throw $e;
        }
        return $shipment;
    }

    public function getItemPrice(Request $request)
    {
        return CustomerUnitPrice::where(
            [
                ['product_id', '=', $request->item],
                ['account_id', '=', $request->account],
                ['start_date', '<=', date('Y-m-d')]
            ]
        )->orderBy('start_date', 'desc')->orderBy('id', 'desc')->with('product')->first();
    }

    /**
     * 削除
     *
     * @param mixed $id
     * @return bool
     */
    public function delete($id)
    {
        \Log::error('aaa');
        DB::beginTransaction();
        try {
            Shipment::where('id', $id)->delete();
            ShipmentLineItem::where('shipment_id', $id)->delete();
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error('社員の削除に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
    }

    /**
     * 検索
     *
     * @param Request $request
     * @return Collection
     */
    public function searchShipment(Request $request)
    {
        return $this->shipmentService->searchByCondition($request->all());
    }

    /**
     * 請求作成のための出荷情報を取得
     *
     * @param Request $request
     * @return array
     */
    public function getListWithAccount(Request $request)
    {
        return $this->shipmentService->getListWithAccount($request->all());
    }

    /**
     * 食数確認表を取得
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function getMealPdf(Request $request)
    {
        $data = $this->shipmentService->getMealPdfData($request->all()['shipments'], $request->all()['start_date'], $request->all()['end_date']);
        try {
            $pdf = PDF::loadView('pdf.meal_form.meal_form', compact('data'))->setOption('page-width', '297')
                ->setOption('page-height', '210');
            return $pdf->stream('sample.pdf');
        } catch (Exception $e) {
            \Log::error('食数確認表の作成に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
    }
    /**
     * 売上一覧表
     *
     * @param integer $id
     * @return string
     */
    public function getListForm(Request $request)
    {
        //TODO 請求一覧表出力処理
        $dataList = $this->shipmentService->getSalesPdfDate($request->all());
        try {
            $pdf = PDF::loadView('pdf.shipment.shipmentListForm', compact('dataList'))->setOption('page-width', '297')
                ->setOption('page-height', '210');
            return $pdf->stream('sample.pdf');
        } catch (Exception $e) {
            \Log::error('食数確認表の作成に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
    }

    public function getHistoryList(int $id)
    {
        return $this->shipmentService->getHistoryList($id);
    }
}