<?php

namespace App\Http\Controllers\Tax;

use App\Http\Controllers\Controller;
use App\Models\Tax;

/**
 * 消費税に関するコントローラー
 */
class TaxController extends Controller
{

    /**
     * デフォルトの消費税情報を取得
     *
     * @return void
     */
    public function getDefaultTax()
    {
        return Tax::where('default_flg', 1)->get()->first();
    }
}