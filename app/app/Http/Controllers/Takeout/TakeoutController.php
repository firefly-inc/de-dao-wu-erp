<?php

namespace App\Http\Controllers\Takeout;

use App\Http\Controllers\Controller;
use App\Http\Service\TakeoutService;
use App\Models\Takeout;
use Illuminate\Http\Request;

/**
 * 持ち出しに関するコントローラー
 */
class TakeoutController extends Controller
{

    protected $takeoutService;

    public function __construct(TakeoutService $takeoutService)
    {
        $this->takeoutService = $takeoutService;
    }

    /**
     * 一覧画面用
     *
     * @param Request $request
     * @return void
     */
    public function getList(Request $request)
    {
        $takeout = $this->takeoutService->getList($request->all())->limit(200)->get();
        $work = [];
        foreach ($takeout as $data) {

            $data->take_out_staff_name = $data->user->last_name . " " . $data->user->first_name;
        }
        return $takeout;
    }

    /**
     * 詳細画面用
     *
     * @param int $id
     * @return void
     */
    public function getDetail(int $id)
    {
        return array('meta' => Takeout::getFieldType(), 'data' => $this->takeoutService->getDetail($id));
    }

    /**
     * 登録処理
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {
        $this->takeoutService->create($request->all());
    }

    /**
     * 更新
     *
     * @param Request $request
     * @return void
     */
    public function update(Request $request)
    {
        $this->takeoutService->update($request->all());
    }

    /**
     * サマリー情報の取得
     *
     * @param Request $request
     * @return void
     */
    public function getSummary(Request $request)
    {
        return $this->takeoutService->getSummary($request->all());
    }

    /**
     * 存在チェック
     *
     * @param integer $user_id
     * @return void
     */
    public function exists(int $user_id)
    {
        return $this->takeoutService->getList(array('take_out_staff_id' => $user_id, 'take_out_date' => date('Y-m-d')))->exists();
    }
}
