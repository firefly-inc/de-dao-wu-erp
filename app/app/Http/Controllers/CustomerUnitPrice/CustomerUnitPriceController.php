<?php

namespace App\Http\Controllers\CustomerUnitPrice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CustomerUnitPrice;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * 得意先単価の関するコントローラー
 */
class CustomerUnitPriceController extends Controller
{
    /**
     * IDをもとに情報を取得
     *
     * @param mixed $id
     * @return array
     */
    public function index($id)
    {
        return array('meta' => CustomerUnitPrice::getFieldType(), 'data' => CustomerUnitPrice::with('account')->with('product')->find($id));
    }

    /**
     * 得意先単価フォームに必要なJSONを取得
     *
     * @return array
     */
    public function getMeta()
    {
        return array('meta' => CustomerUnitPrice::getFieldType());
    }

    /**
     * 検索
     *
     * @param mixed $word
     * @return array
     */
    public function search($word)
    {
        return array('meta' => CustomerUnitPrice::getFieldType(), 'data' => CustomerUnitPrice::where('product_name', 'like', '%' . $word . '%')->get());
    }

    /**
     * 一覧取得
     *
     * @return array
     */
    public function getList()
    {
        return array('meta' => CustomerUnitPrice::getFieldType(), 'data' => CustomerUnitPrice::all());
    }

    /**
     * 更新
     *
     * @param Request $request
     * @param CustomerUnitPrice $customerUnitPrice
     * @return bool
     */
    public function save(Request $request, CustomerUnitPrice $customerUnitPrice)
    {
        DB::beginTransaction();
        try {
            $customerUnitPrice = CustomerUnitPrice::find($request->id);
            $customerUnitPrice->fill($request->all())->save();
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error('得意先単価の更新に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
    }

    /**
     * 登録
     *
     * @param Request $request
     * @param CustomerUnitPrice $customerUnitPrice
     * @return bool
     */
    public function post(Request $request, CustomerUnitPrice $customerUnitPrice)
    {
        DB::beginTransaction();
        try {
            $customerUnitPrice->create($request->all());
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error('得意先単価の登録に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
    }
}