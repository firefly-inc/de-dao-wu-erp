<?php

namespace App\Http\Controllers\DeliveryDestination;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DeliveryDestination;
use Exception;
use App\Http\Service\DeliveryDestinationService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * 納入先に関するコントローラー
 */
class DeliveryDestinationController extends Controller
{

    private $deliveryDestinationService;


    public function __construct(DeliveryDestinationService $deliveryDestinationService)
    {
        $this->deliveryDestinationService = $deliveryDestinationService;
    }

    /**
     * IDをもとに情報取得
     *
     * @param mixed $id
     * @return array
     */
    public function index($id)
    {
        return array('meta' => DeliveryDestination::getFieldType(), 'data' => DeliveryDestination::find($id));
    }

    /**
     * 納入先フォームの作成に必要なJSON情報を取得
     *
     * @return array
     */
    public function getMeta()
    {
        return array('meta' => DeliveryDestination::getFieldType());
    }

    /**
     * IDをもとに検索した情報と、JSON情報を取得
     *
     * @param mixed $id
     * @return array
     */
    public function getDeliveryDestinationsList($id)
    {
        return array('data' => DeliveryDestination::where('id', '=', $id)->get());
    }

    public function getPriceList($id)
    {
        return array('data' => DeliveryDestination::where('id', '=', $id)->get());
    }

    /**
     * 検索
     *
     * @param mixed $word
     * @return array
     */
    public function search($word)
    {
        return array('meta' => DeliveryDestination::getFieldType(), 'data' => DeliveryDestination::orWhere('name', 'like', '%' . $word . '%')->orWhere('id', 'like', '%' . $word . '%')->limit(20)->get());
    }

    /**
     * 検索
     *
     * @param int $account_id
     * @param mixed $word
     * @return array
     */
    public function searchForAccount($account_id, $word)
    {
        return array('meta' => DeliveryDestination::getFieldType(), 'data' => DeliveryDestination::where(function ($q) use ($word) {
            $q->orWhere('name', 'like', '%' . $word . '%')->orWhere('id', 'like', '%' . $word . '%');
        })->where('account_id', $account_id)->limit(20)->get());
    }

    /**
     * 全権取得
     *
     * @return array
     */
    public function getList()
    {
        return array('meta' => DeliveryDestination::getFieldType(), 'data' => DeliveryDestination::limit(100)->get());
    }

    /**
     * 更新
     *
     * @param Request $request
     * @return bool
     */
    public function save(Request $request)
    {
        DB::beginTransaction();
        try {
            DeliveryDestination::find($request->id)->fill($request->all())->save();
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error('納入先の更新に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
    }

    /**
     * 登録
     *
     * @param Request $request
     * @param DeliveryDestination $deliveryDestination
     * @return Model
     */
    public function post(Request $request,  DeliveryDestination $deliveryDestination)
    {
        DB::beginTransaction();
        try {
            $deliveryDestination->create($request->all());
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error('納入先の登録に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
    }

    /**
     * 削除
     *
     * @param mixed $id
     * @return bool
     */
    public function delete($id)
    {
        DB::beginTransaction();
        try {
            DeliveryDestination::where('id', $id)->delete();
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error('納入先の削除に失敗しました。');
            \Log::error($e->getMessage());
            throw $e;
        }
    }

    /**
     * 検索
     *
     * @param Request $request
     * @return Collection
     */
    public function searchDeliveryDestination(Request $request)
    {
        \Log::alert("message112");
        return $this->deliveryDestinationService->searchByCondition($request->all());
    }
}