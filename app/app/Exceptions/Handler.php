<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function render($request, Throwable $e)
    {
        // どの例外クラスが発生したかによって処理を分けられる。
        if ($e instanceof DuplicateException) {
            \Log::error('処理に失敗しました。');
            \Log::error($e->getMessage() . ':' . $e->getFile() . ':' . $e->getCode() . ':' . $e->getLine());
            return response()->json('商品情報が重複しています', 500);
        }
        if ($e instanceof Exception) {
            \Log::error('処理に失敗しました。');
            \Log::error($e->getMessage() . ':' . $e->getFile() . ':' . $e->getCode() . ':' . $e->getLine());
            return response()->json('例外が発生しました。', 500);
        }
        return parent::render($request, $e);
    }
}