<?php

namespace App\Models;
use App\Traits\PaymentObservable;
class Payment extends BaseModel
{
    use PaymentObservable;
    protected $table = 'payments';
    protected $fillable = [
        'deposit_amount', 'payment_date','invoice_id','payments_class','remarks','staff_id'
    ];
    protected $hidden = ['deleted_at'];
    public static function getFieldType()
    {
        return json_decode(file_get_contents(__DIR__ . '/fieldType/Payment.json'), true);
    }
    public function getStaffNameAttribute()
    {
        if (empty($this->Staff)) {
            return "";
        } else {
            return $this->Staff->name;
        }
    }
    public function toArray()
    {
        return array_merge(
            parent::toArray(),
            [
                'staff_name' => $this->staff_name,
                'staff' => $this->staff,
                'invoice' => $this->invoice,
                'account_name'=>$this->account_name
            ]
        );
    }

    public function getAccountNameAttribute()
    {
        if (empty($this->invoice)) {
            return "";
        }else{
            return $this->invoice->account_name;
        }

    }
    public function Staff()
    {
        return $this->belongsTo(User::class, 'staff_id', 'id');
    }
    public function Invoice()
    {
        return $this->belongsTo(Invoice::class, 'invoice_id', 'id');
    }
}