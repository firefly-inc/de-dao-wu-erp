<?php

namespace App\Models;

class Product extends BaseModel
{
    protected $table = 'products';
    protected $fillable = [
        'name',
        'Class', 'Inventory_unit',
        'purchase_unit_price',
        'unit_price',
        'taxation_flg',
        'tax',
        'created_by',
        'updated_by',
        'deleted_by'
    ];
    protected $hidden = ['deleted_at', 'MiddleCategory', 'SmallCategory', 'BigCategory'];
}