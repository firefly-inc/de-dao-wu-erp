<?php

namespace App\Models;

use App\Models\BaseModel;
use Carbon\Carbon;

class Account extends BaseModel
{
    protected $fillable = [
        'fax',
        'second_adress',
        'first_adress',
        'fax',
        'type',
        'Abbreviation',
        'kana',
        'postal_code',
        'name',
        'billing_id',
        'phone_number',
        'TaxType',
        'deadline',
        'staff_id',
        'payment_day',
        'payment_month_type',
        'collect_class',
        'display_carried_forward',
        'TotalBillingAmount',
        'TotalPayment',
        'kurikoshi'
    ];
    public function billingAccount()
    {
        return $this->hasOne(Account::class, 'id', 'billing_id');
    }

    public function getAdressAttribute()
    {
        return $this->first_adress . $this->second_adress;
    }

    //累計請求金額
    public function getKurikoshi()
    {
        if($this->display_carried_forward){
            return  $this->total_billing_amount - $this->total_payment_date;
        }else{
            return 0;
        }
    }

    //累計請求金額
    public function getTotalBillingAmount()
    {
        $count = 0;
        if (!empty($this->invoices)){
            foreach ($this->invoices as $invoice) {
                $count += $invoice->total;
            }
        }
        return $count;
    }
    //累計支払金額
    public function getTotalPayment()
    {

        $count = 0;
        if (!empty($this->invoices)){
            foreach ($this->invoices as $invoice) {

                $count += $invoice->deposited_amount;
            }
        }
        return $count;
    }

    public function shipments()
    {
        return $this->hasMany(Shipment::class);
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function staff()
    {
        return $this->belongsTo(User::class, 'staff_id', 'id');
    }
    public function driver()
    {
        return $this->belongsTo(User::class, 'driver_id', 'id');
    }

    public function shipmentsSalesDate($month)
    {
        $firstDate = date('Y-m-d', strtotime('first day of ' . $month));
        $lastDate = date('Y-m-d', strtotime('last day of ' . $month));
        return $this->hasMany(Shipment::class)->where('sales_date', '>=', $firstDate)->where('sales_date', '<=', $lastDate);
    }

    public function getSubtotalAttribute()
    {
        $subtotal = 0;
        foreach ($this->shipments as $shipment) {
            $subtotal += $shipment->subtotal;
        }
        return $subtotal;
    }

    public function getStaffNameAttribute()
    {

        if (empty($this->staff->last_name)) return '';
        return $this->staff->last_name;
    }
    public function getDriverNameAttribute()
    {

        if (empty($this->driver->last_name)) return '';
        return $this->driver->last_name;
    }

    public function toArray()
    {
        return array_merge(
            parent::toArray(),
            [
                'subtotal' => $this->subtotal,
                'staff_name' => $this->staff_name,
                'staff' => $this->staff,
                'driver_name' => $this->driver_name,
                'driver' => $this->driver,
            ]
        );
    }

    public static function boot()
    {
        parent::boot();

        self::creating(function (Account $account) {
            if (empty($account->billing_id)) {
                $account->billing_id = $account->id;
            } else {
            }
        });
    }
}
