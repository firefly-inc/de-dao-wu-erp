<?php

namespace App\Models;
use App\Traits\InvoiceObservable;
use Carbon\Carbon;

class Invoice extends BaseModel
{
    use InvoiceObservable;
    protected $table = 'invoices';
    protected $fillable = [
        'account_id', 'payment_date', 'billing_date', 'deposited', 'starting_date', 'deadline', 'payments_class','subtotal','tax','deposited_amount'
    ];


    public function toArray()
    {
        return array_merge(
            parent::toArray(),
            [
                'account_name' => $this->account_name,
                'total' => $this->total,
                'collect_class' => $this->collect_class
            ]
        );
    }

    //==
    //出荷情報
    //==
    public function Shipments()
    {
        return $this->hasMany(Shipment::class);
    }

    //==
    //出荷情報
    //==
    public function ShipmentsOrderByDeliveryDestination()
    {
        return $this->Shipments()->orderBy('sales_date')->get();
    }


    //小計金額取得処理
    public function getSubtotal()
    {
        $count = 0;
        foreach ($this->Shipments as $shipment) {
            $count += $shipment->subtotal;
        }
        // 内税商品の消費税分をマイナスに
        $count - $this->getTax();
        return $count;
    }

    //消費税取得処理
    public function getTax()
    {
        $count = 0;
        foreach ($this->Shipments as $shipment) {
            $count += $shipment->tax;
        }
        return $count;
    }

    /**
     * 内税のみの合計を取得
     *
     * @return int
     */
    public function getTaxIncludedAttribute()
    {
        $count = [
            '内税' => [],
            '外税' => [],
            '非課税' => []
        ];
        foreach ($this->Shipments as $shipment) {
            foreach ($shipment->ShipmentLineItems as $ShipmentLineItem) {
                // nullがtaxに入っている場合、計算しない
                if (empty($ShipmentLineItem->tax)) {
                    continue;
                }
                if (array_key_exists($ShipmentLineItem->tax, $count[$ShipmentLineItem->TaxType])) {
                    array_push($count[$ShipmentLineItem->TaxType][$ShipmentLineItem->tax], [
                        'price' => $ShipmentLineItem->unit_price,
                        'quantity' => $ShipmentLineItem->quantity
                    ]);
                } else {
                    $count[$ShipmentLineItem->TaxType][$ShipmentLineItem->tax] = array(['price' => $ShipmentLineItem->unit_price, 'quantity' => $ShipmentLineItem->quantity]);
                }
            }
        }
        $response = 0;
        foreach ($count as $tax_type => $data) {
            foreach ($data as $tax => $item) {
                foreach ($item as $i => $value) {
                    switch ($tax_type) {
                        case '内税':
                            $response += floor(($value['price'] * $tax / ($tax + 1)) * $value['quantity']);
                            break;
                        case '外税':
                            break;
                        case '非課税':
                            break;
                    }
                }
            }
        }
        return $response;
    }

    //合計金額取得処理
    public function getTotalAttribute()
    {
        return $this->subtotal + $this->tax;
    }

    // 入金
    public function Payments()
    {
        return $this->hasMany(Payment::class);
    }
    //支払済み金額
    public function getDepositedAmount()
    {
        $count = 0;
        $Payments = $this->Payments;
        foreach ($Payments as $Payment) {
            $count += ($Payment->deposit_amount);
        }
        return $count;
    }


    //得意先
    public function Account()
    {
        return $this->belongsTo(Account::class, 'account_id', 'id');
    }
    public function getAccountNameAttribute()
    {
        if (empty($this->account)) {
            return "";
        }else{
            return $this->account->name;
        }
    }

    public function getCollectClassAttribute()
    {
        if (empty($this->account)) {
            return "";
        }else{
            return $this->account->collect_class;
        }
    }

    // 請求書
    public function invoiceForm()
    {
        return $this->hasMany(InvoiceForm::class);
    }
}
