<?php

namespace App\Models;

use Carbon\Carbon;
use App\Traits\ShipmentObservable;

class Shipment extends BaseModel
{
    use ShipmentObservable;
    protected $fillable = [
        'account_id',
        'delivery_destination_id',
        'sales_employee_id',
        'delivery_employee_id',
        'remarks',
        'sales_date',
        'ship_date',
        'individual_invoice_flg',
        'invoice_id',
        'cash_receipt'
    ];

    protected $hidden = ['deleted_at'];
    public static function getFieldType()
    {
        return json_decode(file_get_contents(__DIR__ . '/fieldType/Shipment.json'), true);
    }
    public function getAccountNameAttribute()
    {
        if (empty($this->account)) {
            return '';
        } else {
            return $this->account->name;
        }
    }
    public function getDeliveryEmployeeNameAttribute()
    {
        if (empty($this->DeliveryEmployee)) {
            return '';
        } else {
            return $this->DeliveryEmployee->last_name;
        }
    }
    public function getSalesEmployeeNameAttribute()
    {
        if (empty($this->SalesEmployee)) {
            return '';
        } else {
            return $this->SalesEmployee->last_name;
        }
    }
    public function getDeliveryDestinationNameAttribute()
    {
        if (empty($this->DeliveryDestination)) {
            return "";
        } else {
            return $this->DeliveryDestination->name;
        }
    }

        //小計金額取得処理
        public function getTax()
        {
            $count = 0;
            $ShipmentLineItems = $this->ShipmentLineItems;
            foreach ($ShipmentLineItems as $ShipmentLineItem) {
                $count += $ShipmentLineItem->tax_amount;
            }
            return $count;
        }

    //請求済みかどうか
    public function getBilledAttribute()
    {
        if (empty($this->Invoice)) {
            return false;
        } else {
            return true;
        }
    }

    //請求済みかどうか
    public function getStatusAttribute()
    {
        if (empty($this->Invoice)) {
            return "出荷済";
        } else {
            if ($this->Invoice->deposited == 0) {
                return "請求済";
            } else {
                return "入金確認済";
            }
        }
    }
    public function toArray()
    {
        return array_merge(
            parent::toArray(),
            [
                'account_name' => $this->account_name,
                'account' => $this->Account,
                'sales_employee_name' => $this->sales_employee_name,
                'sales_employee' => $this->salesEmployee,
                'delivery_destination_name' => $this->delivery_destination_name,
                'delivery_destination' => $this->DeliveryDestination,
                'delivery_employee' => $this->DeliveryEmployee,
                'delivery_employee_name' => $this->delivery_employee_name,
                'billed' => $this->billed,
                'shipment_line_items' => $this->ShipmentLineItems,
                'status' => $this->status
            ]
        );
    }
    public function ShipmentLineItems()
    {
        return $this->hasMany(ShipmentLineItem::class);
    }

    public function Account()
    {
        return $this->belongsTo(Account::class, 'account_id', 'id');
    }

    public function Invoice()
    {
        return $this->belongsTo(Invoice::class, 'invoice_id', 'id');
    }

    public function DeliveryDestination()
    {
        return $this->belongsTo(DeliveryDestination::class, 'delivery_destination_id');
    }
    public function SalesEmployee()
    {
        return $this->belongsTo(User::class, 'sales_employee_id', 'id');
    }
    public function DeliveryEmployee()
    {
        return $this->belongsTo(User::class, 'delivery_employee_id', 'id');
    }
    public static function boot()
    {
        parent::boot();
    }
}