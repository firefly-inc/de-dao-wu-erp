<?php

namespace App\Models;

class DeliveryDestination extends BaseModel
{
    protected $fillable = [
        'name', 'account_id', 'unit_price', 'start_date', 'fax', 'type', 'kana', 'phone_number', 'postal_code', 'adress', 'text', 'first_adress', 'second_adress'
    ];

    protected $hidden = ['deleted_at'];
    public static function getFieldType()
    {
        return json_decode(file_get_contents(__DIR__ . '/fieldType/DeliveryDestination.json'), true);
    }

    public function toArray()
    {
        return array_merge(
            parent::toArray(),
            [
                'account' => $this->Account,
                'account_name' => $this->account_name,
            ]
        );
    }

    public function getAccountNameAttribute()
    {

        if (empty($this->Account)) return '';
        return $this->Account->name;
    }

    public function Account()
    {
        return $this->belongsTo(Account::class, 'account_id', 'id');
    }
}