<?php

namespace App\Models;

class Takeout extends BaseModel
{

    protected $hidden = ['deleted_at'];
    protected $fillable = [
        'id',
        'take_out_staff_id',
        'take_out_date',
    ];

    public function takeoutDetail()
    {
        return $this->hasMany(TakeoutDetail::class,'take_out_id','id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'take_out_staff_id');
    }
}