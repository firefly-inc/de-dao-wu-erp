<?php

namespace App\Models;

use App\Traits\EloquentObservable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class BaseModel extends Model
{
    use HasFactory, SoftDeletes, EloquentObservable;

    /**
     * モデルのJSONファイルを取得
     *
     * @return mixed
     */
    public static function getFieldType()
    {
        $modelName = explode('\\', get_called_class());
        return json_decode(file_get_contents(__DIR__ . '/fieldType/' . end($modelName) . '.json'), true);
    }

    /**
     * created_atの出力形式を変更
     *
     * @return string
     */
    public function getUpdatedAtAttribute()
    {
        return empty($this->attributes['updated_at'])? '' :  Carbon::parse($this->attributes['updated_at'])->format('Y年m月d日H:i');
    }

    /**
     * updated_atの出力形式を変更
     *
     * @return string
     */
    public function getCreatedAtAttribute()
    {
        return empty($this->attributes['created_at'])? '' : Carbon::parse($this->attributes['created_at'])->format('Y年m月d日H:i');
    }
}