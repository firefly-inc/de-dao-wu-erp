<?php

namespace App\Models;
use App\Traits\ShipmentLineItemObservable;

class ShipmentLineItem extends BaseModel
{

    use ShipmentLineItemObservable;

    protected $hidden = ['deleted_at'];
    protected $fillable = [
        'id',
        'shipment_id',
        'product_id',
        'unit_price',
        'quantity',
        'TaxType',
        'tax',
    ];

    public function getProductNameAttribute()
    {
        return $this->product->name;
    }

    public function getSubTotalAttribute()
    {
        if(trim($this->TaxType) == "内税"){
            return ($this->quantity * $this->unit_price) - $this->tax_amount;
        }else{
            return $this->quantity * $this->unit_price;
        }
    }

    public function getTaxAmountAttribute()
    {
        if(trim($this->TaxType) == '内税'){
            return $this->ceil_plus(($this->quantity * $this->unit_price) *  $this->product->tax /($this->product->tax + 1),6);
        }else if($this->TaxType == '非課税'){
            return 0;
        }else{
            return  ($this->quantity * $this->unit_price) * $this->product->tax;
        }

    }

    public function toArray()
    {
        return array_merge(
            parent::toArray(),
            [
                'subtotal' => floor($this->subtotal),
                'product_name' => $this->product_name,
                'product' => $this->product
            ]
        );
    }


    public function shipment()
    {
        return $this->belongsTo(Shipment::class, 'shipment_id', 'id');
    }
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    function ceil_plus(float $value, ?int $precision = null): float
    {
      if (null === $precision) {
        return (float) ceil($value);
      }
      if ($precision < 0) {
        throw new \RuntimeException('Invalid precision');
      }

      $reg = $value + 0.5 / (10 ** $precision);
      return round($reg, $precision, $reg > 0 ? PHP_ROUND_HALF_DOWN : PHP_ROUND_HALF_UP);
    }
}
