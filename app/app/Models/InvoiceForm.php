<?php

namespace App\Models;

class InvoiceForm extends BaseModel
{

    protected $table = 'invoices_forms';
    protected $fillable = [
        'pdf_path', 'invoice_id'
    ];

    // 請求
    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    public function getPdflinkAttribute()
    {
        return config('constant.app_url') . '/' . $this->pdf_path;
    }

    public function toArray()
    {
        return array_merge(
            parent::toArray(),
            [
                'pdf_link' => $this->pdflink,
            ]
        );
    }
}