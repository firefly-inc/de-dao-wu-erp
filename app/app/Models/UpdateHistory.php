<?php

namespace App\Models;

use App\Models\BaseModel;
use Carbon\Carbon;

class UpdateHistory extends BaseModel
{
    protected $table = 'update_histories';

    protected $fillable = [
        'id',
        'update_table_name',
        'update_table_id',
        'old_value',
        'new_value',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];

    /**
     * ネイティブなタイプへキャストする属性
     *
     * @var array
     */
    protected $casts = [
        'old_value' => 'array',
        'new_value' => 'array',
    ];
}