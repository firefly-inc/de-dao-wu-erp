<?php

namespace App\Models;

use Carbon\Carbon;
class Department extends BaseModel
{
    protected $table = 'departments';
    protected $fillable = [
        'name',
        'restriction_flg'
    ];
    public function getUpdatedAtAttribute()
    {
        return Carbon::parse($this->attributes['updated_at'])->format('Y年m月d日H:i');
    }
    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->format('Y年m月d日H:i');
    }
}