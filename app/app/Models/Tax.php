<?php

namespace App\Models;
class Tax extends BaseModel
{

    protected $table = 'tax';

    protected $fillable = [
        'percent',
        'start_date',
        'end_date',
        'default_flg',
    ];

    protected $hidden = ['deleted_at'];

}
