<?php

namespace App\Models;

class CustomerUnitPrice extends BaseModel
{

    protected $fillable = [
        'product_id', 'account_id', 'unit_price', 'start_date', 'TaxType'
    ];
    protected $guarded = ['account', 'product'];

    protected $hidden = ['deleted_at'];
    public static function getFieldType()
    {
        return json_decode(file_get_contents(__DIR__ . '/fieldType/CustomerUnitPrice.json'), true);
    }

    public function getAccountNameAttribute()
    {
        if(empty($this->account)){
            return "";
        }else{
            return $this->account->name;
        }
    }
    public function getProductNameAttribute()
    {
        if(empty($this->product)){
            return "";
        }else{
            return $this->product->name;
        }

    }

    public function toArray()
    {
        return array_merge(
            parent::toArray(),
            [
                'account_name' => $this->account_name,
                'product_name' => $this->product_name
            ]
        );
    }

    public function account()
    {
        return $this->belongsTo(Account::class, 'account_id', 'id');
    }
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}