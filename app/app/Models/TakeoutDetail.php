<?php

namespace App\Models;

class TakeoutDetail extends BaseModel
{
    protected $hidden = ['deleted_at'];
    protected $fillable = [
        'id',
        'take_out_id',
        'product_id',
        'quantity',
    ];

    public function takeout()
    {
        return $this->belongsTo(Takeout::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}