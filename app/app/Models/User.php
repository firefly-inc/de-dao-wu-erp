<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'last_name', 'first_name', 'last_name_kana', 'first_name_kana', 'code','department_id','is_delivery_staff'
    ];

    public function getNameAttribute()
    {
        return $this->last_name . ' ' . $this->first_name;
    }

    public function toArray()
    {
        return array_merge(
            parent::toArray(),
            ['name' => $this->name,
            'department_name' => $this->department_name,
            'department' => $this->Department,
            'restriction_flg' => $this->restriction_flg,
            ]
        );
    }

    public function getDepartmentNameAttribute()
    {
        if (empty($this->Department)) {
            return "";
        } else {
            return $this->Department->name;
        }
    }

    public function getRestrictionFlgAttribute()
    {
        if (empty($this->Department)) {
            return 0;
        } else {
            return $this->Department->restriction_flg;
        }
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function getFieldType()
    {
        return json_decode(file_get_contents(__DIR__ . '/fieldType/User.json'), true);
    }
    public function Department()
    {
        return $this->belongsTo('App\Models\Department');
    }

    /**
     * JWT の subject claim となる識別子を取得する
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * キーバリュー値を返します, JWTに追加される custom claims を含む
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getUpdatedAtAttribute()
    {
        return Carbon::parse($this->attributes['updated_at'])->format('Y年m月d日H:i');
    }
    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->format('Y年m月d日H:i');
    }
}