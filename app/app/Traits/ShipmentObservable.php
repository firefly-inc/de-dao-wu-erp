<?php

namespace App\Traits;

use App\Observers\ShipmentObserver;
use Illuminate\Database\Eloquent\Model;

trait ShipmentObservable
{
    public static function bootShipmentObservable()
    {
        self::observe(ShipmentObserver::class);
    }
}