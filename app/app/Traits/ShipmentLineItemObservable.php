<?php

namespace App\Traits;

use App\Observers\ShipmentLineItemObserver;
use Illuminate\Database\Eloquent\Model;

trait ShipmentLineItemObservable
{
    public static function bootShipmentLineItemObservable()
    {
        self::observe(ShipmentLineItemObserver::class);
    }
}