<?php

namespace App\Traits;

use App\Observers\EloquentObserver;
use Illuminate\Database\Eloquent\Model;

trait EloquentObservable
{
    public static function bootEloquentObservable()
    {
        self::observe(EloquentObserver::class);
    }
}