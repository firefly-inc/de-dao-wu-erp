<?php

namespace App\Traits;

use App\Observers\PaymentObserver;
use Illuminate\Database\Eloquent\Model;

trait PaymentObservable
{
    public static function bootPaymentObservable()
    {
        self::observe(PaymentObserver::class);
    }
}