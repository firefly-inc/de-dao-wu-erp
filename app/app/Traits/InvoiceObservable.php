<?php

namespace App\Traits;

use App\Observers\InvoiceObserver;
use Illuminate\Database\Eloquent\Model;

trait InvoiceObservable
{
    public static function bootInvoiceObservable()
    {
        self::observe(InvoiceObserver::class);
    }
}
