<?php

namespace App\Listeners;

use App\Events\AccountCreating;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CreateDeliveryFestination
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AccountCreating  $event
     * @return void
     */
    public function handle(AccountCreating $event)
    {
        //
    }
}
