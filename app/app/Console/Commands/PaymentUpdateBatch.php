<?php

namespace App\Console\Commands;

use App\Models\Account;
use App\Models\Invoice;
use App\Models\Payment;
use App\Models\Shipment;
use Carbon\Carbon;
use Illuminate\Console\Command;

class PaymentUpdateBatch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:payment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '入金更新用バッチ処理';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $list = Payment::where('created_at', '2021-08-31 00:00:00')->get();
        foreach ($list as $data) {
            if($data->deposit_amount != null){
                $data->deposit_amount += 1;
                $data->save();
                $data->deposit_amount -= 1;
                $data->save();
            }else{
                $data->deposit_amount = 1;
                $data->save();
                $data->deposit_amount = null;
                $data->save();
            }
        }
        \Log::debug(count($list));
    }
}