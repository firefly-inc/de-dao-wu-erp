<?php

namespace App\Console\Commands;

use App\Models\Account;
use App\Models\Invoice;
use App\Models\Shipment;
use Carbon\Carbon;
use Illuminate\Console\Command;

class InvoiceBatch2 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:invoice2';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '請求情報データ移行用バッチ処理';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \Log::debug("起動");
        $dataList = Shipment::
            where('sales_date', '<=', '2021-06-31')
            ->limit(100)
            ->get();
            foreach ($dataList as $shipment) {
                $shipment->invoice_id = null;
                    $shipment->update();
            }

    }

}