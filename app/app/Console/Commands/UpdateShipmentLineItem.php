<?php

namespace App\Console\Commands;

use App\Models\Account;
use App\Models\Invoice;
use App\Models\Shipment;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateShipmentLineItem extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:updateshipment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '出荷詳細空更新バッチ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $shipmentList = Shipment::all();
        foreach ($shipmentList as $shipment) {
            foreach ($shipment->ShipmentLineItems as $shipmentLineItem) {
                $shipmentLineItem->quantity += 1;
                $shipmentLineItem->update();
                $shipmentLineItem->quantity -= 1;
                $shipmentLineItem->update();
                break;
            }
        }
    }
}