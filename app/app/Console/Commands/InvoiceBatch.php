<?php

namespace App\Console\Commands;

use App\Models\Account;
use App\Models\Invoice;
use App\Models\Shipment;
use Carbon\Carbon;
use Illuminate\Console\Command;

class InvoiceBatch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:invoice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '請求情報データ移行用バッチ処理';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \Log::debug("起動");
        $count = Shipment::whereNull('invoice_id')
            ->where('sales_date', '<=', '2021-08-31')
            ->count();
        for ($i = 1; $count >= $i; $i += 1000) {
            $dataList = Shipment::whereNull('invoice_id')
                ->where('sales_date', '<=', '2021-08-31')
                ->limit(1000)
                ->get();

            // 得意先ごとに分ける
            $dataAccountList = array();
            foreach ($dataList as $data) {
                if (array_key_exists($data->account_id, $dataAccountList)) {
                    array_push($dataAccountList[$data->account_id], $data);
                } else {
                    $dataAccountList[$data->account_id] = array($data);
                }
            }

            // さらに請求日ごとに分ける
            $dataAccountDateList = array();
            $updateShipmentList = array();
            foreach ($dataAccountList as $accountId => $list) {
                $account = Account::find($accountId);
                if (!array_key_exists($accountId, $dataAccountDateList)) {
                    $dataAccountDateList += array($accountId => array());
                }
                foreach ($list as $data) {
                    $billingDate = $this->getInvoiceDate($account->deadline, $data->sales_date);
                    if (array_key_exists($billingDate, $dataAccountDateList[$accountId])) {
                        array_push($dataAccountDateList[$accountId][$billingDate], $data);
                    } else {
                        $dataAccountDateList[$accountId][$billingDate] = array($data);
                        array_push($updateShipmentList, $data);
                    }
                }
            }

            // 請求データを作る
            foreach ($dataAccountDateList as $accountId => $dateList) {
                foreach ($dateList as $date => $sList) {
                    $invoice = $this->createInvoiceDate($date, $accountId);
                    foreach ($sList as $shipment) {
                        $shipment->remarks = '2021-12-08test';
                        $shipment->invoice_id = $invoice->id;
                        $shipment->update();
                    }
                }
            }
        }
    }

    /**
     * 得意先に締日と出荷日をもとに請求日を取得
     *
     * @param [type] $deadline
     * @param [type] $shipDate
     * @return static
     */
    private function getInvoiceDate($deadline, $shipDate)
    {
        \Log::debug($shipDate);
        \Log::debug($deadline);
        $date = new Carbon($shipDate);
        if ($deadline == '末日') {
            \Log::debug("105" . $date->endOfMonth()->format('Y-m-d'));
            return $date->endOfMonth()->format('Y-m-d');
        } else if ($date->day <= $deadline) {
            \Log::debug("108" . (new Carbon($date->year . '-' . $date->month . '-' . $deadline))->format('Y-m-d'));
            return (new Carbon($date->year . '-' . $date->month . '-' . $deadline))->format('Y-m-d');
        } else {
            \Log::debug("111" . (new Carbon($date->year . '-' . ($date->month) . '-' . $deadline))->addMonth(1)->format('Y-m-d'));
            return (new Carbon($date->year . '-' . ($date->month) . '-' . $deadline))->addMonth(1)->format('Y-m-d');
        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $data
     * @param [type] $invoiceDate
     * @return Invoice
     */
    private function createInvoiceDate($date, $accountId)
    {
        $account = Account::find($accountId);
        $starting_date = '';
        \Log::debug("127~" . $date);
        $invoiceDate = new Carbon($date);
        if ($account->deadline == "末日") {
            $starting_date = (new Carbon($date))->startOfMonth();
        } else {
            $starting_date = (new Carbon($date))->subMonth(1)->addDay();
        }
        \Log::debug("134~" . $invoiceDate->format("Y-m-d"));
        return Invoice::create([
            'billing_date' => $invoiceDate,
            'account_id' => $accountId,
            'deadline' => $invoiceDate,
            'starting_date' => $starting_date,
            'payments_class' => '999999',
            'payment_date' => $this->getPaymentDate($account, new Carbon($date))
        ]);
    }

    private function getPaymentDate(Account $account, $date)
    {
        $date = $date->startOfMonth();
        switch ($account->payment_month_type) {
            case '翌月':
                $date = $date->addMonth(1);
                break;
            case '翌々月':
                $date = $date->addMonth(2);
                break;
        }
        if ($account->payment_day == "末日") {
            return $date->endOfMonth();
        } else {
            return new Carbon($date->year . '-' . $date->month . '-' . $account->payment_day);
        }
    }
}