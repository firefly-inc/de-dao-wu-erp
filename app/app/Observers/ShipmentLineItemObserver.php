<?php

namespace App\Observers;

use App\Models\Shipment;
use Illuminate\Support\Facades\Auth;
use App\Models\ShipmentLineItem;

class ShipmentLineItemObserver
{
    public function created(ShipmentLineItem $model)
    {
        if(Shipment::where('id', $model['shipment_id'])->first() != null){
            $this->setSubtotal(Shipment::where('id', $model['shipment_id'])->first());
        }
    }
    public function updated(ShipmentLineItem $model)
    {
        if(Shipment::where('id', $model['shipment_id'])->first() != null){
            $this->setSubtotal(Shipment::where('id', $model['shipment_id'])->first());
        }

    }
    public function deleted(ShipmentLineItem $model)
    {
        if(Shipment::where('id', $model['shipment_id'])->first() != null){
            $this->setSubtotal(Shipment::where('id', $model['shipment_id'])->first());
        }
    }

    public function setSubtotal(Shipment $Shipment)
    {
        $d_subtotal = 0;
        $outer_tax = 0.00;
        $inner_tax = 0.00;
        $ShipmentLineItems = $Shipment->ShipmentLineItems;
        foreach ($ShipmentLineItems as $ShipmentLineItem) {
            $d_subtotal +=$ShipmentLineItem->quantity * $ShipmentLineItem->unit_price;
            if (trim($ShipmentLineItem->TaxType) == '外税') {
                $outer_tax += $ShipmentLineItem->tax_amount;
            } else {
                $inner_tax += $ShipmentLineItem->tax_amount;
            }
        }
        $d_subtotal =floor( floor($d_subtotal) - floor($inner_tax));
        $inner_tax = floor($inner_tax);
        $outer_tax = floor($outer_tax);
        $Shipment->subtotal = $d_subtotal;
        $Shipment->tax = $outer_tax + $inner_tax;
        $Shipment->save();
    }

}