<?php

namespace App\Observers;

use App\Models\Payment;
use Illuminate\Support\Facades\Auth;
use App\Models\Invoice;

class PaymentObserver
{
    public function created(Payment $model)
    {
        $Invoice = Invoice::where('id', $model['invoice_id'])->first();
        $Invoice->deposited_amount = $Invoice->getDepositedAmount();
        $Invoice->save();
    }
    public function updated(Payment $model)
    {
        $Invoice = Invoice::where('id', $model['invoice_id'])->first();
        if($Invoice != null){
            $Invoice->deposited_amount = $Invoice->getDepositedAmount();
            $Invoice->save();
        }

    }
    public function deleted(Payment $model)
    {
        $Invoice = Invoice::where('id', $model['invoice_id'])->first();
        if(!empty($Invice)){
            $Invoice->deposited_amount = $Invoice->getDepositedAmount();
            $Invoice->save();
        }

    }
}