<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class EloquentObserver
{
    public function creating(Model $model)
    {
        if (Auth::guard('api')->check()) {
            $model->created_by = Auth::guard('api')->user()->id;
        }
    }
    public function updating(Model $model)
    {
        if (Auth::guard('api')->check()) {
            $model->updated_by = Auth::guard('api')->user()->id;
        }
    }
    public function saving(Model $model)
    {
        if (Auth::guard('api')->check()) {
            $model->updated_by = Auth::guard('api')->user()->id;
        }
    }
    public function deleting(Model $model)
    {
        if (Auth::guard('api')->check()) {
            $model->deleted_by = Auth::guard('api')->user()->id;
        }
    }
    public function restoring(Model $model)
    {
        if (Auth::guard('api')->check()) {
            $model->restored_by = Auth::guard('api')->user()->id;
        }
    }
}