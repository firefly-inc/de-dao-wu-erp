<?php

namespace App\Observers;

use App\Models\Account;
use App\Models\Shipment;
use App\Models\Payment;
use Illuminate\Support\Facades\Auth;
use App\Models\Invoice;

class InvoiceObserver
{
    public function created(Invoice $model)
    {
        $Account = Account::where('id', $model['account_id'])->first();
        $Account->TotalPayment = $Account->getTotalPayment();
        $Account->TotalBillingAmount = $Account->getTotalBillingAmount();
        $Account->kurikoshi = $Account->TotalBillingAmount - $Account->TotalPayment;
        $Account->save();
    }
    public function updated(Invoice $model)
    {
        $Account = Account::where('id', $model['account_id'])->first();
        $Account->TotalPayment = $Account->getTotalPayment();
        $Account->TotalBillingAmount = $Account->getTotalBillingAmount();
        $Account->kurikoshi = $Account->TotalBillingAmount - $Account->TotalPayment;
        $Account->save();
    }
    public function deleted(Invoice $model)
    {
        $Account = Account::where('id', $model['account_id'])->first();
        $Account->TotalPayment = $Account->getTotalPayment();
        $Account->TotalBillingAmount = $Account->getTotalBillingAmount();
        $Account->kurikoshi = $Account->TotalBillingAmount - $Account->TotalPayment;
        $Account->save();

        $Shipments = Shipment::where('invoice_id', $model['id'])->get();
        foreach ($Shipments as $shipment) {
            $shipment->invoice_id=null;
            $shipment->save();
        }
        $Payments = Payment::where('invoice_id', $model['id'])->get();
        foreach ($Payments as $Payment) {
            $Payment->delete();
        }
    }
}