<?php

namespace App\Observers;

use Illuminate\Support\Facades\Auth;
use App\Models\Shipment;
use App\Models\Invoice;
class ShipmentObserver
{
    public function created(Shipment $model)
    {
        if(!empty($model['invoice_id'])){
            if (Invoice::where('id', $model['invoice_id'])->first() != null) {
                $this->setSubtotal(Invoice::where('id', $model['invoice_id'])->first());
            }
        }
    }
    public function updated(Shipment $model)
    {
        if (!empty($model['invoice_id'])) {
            if (Invoice::where('id', $model['invoice_id'])->first() != null) {
                $this->setSubtotal(Invoice::where('id', $model['invoice_id'])->first());
            }
        }
    }
    public function deleted(Shipment $model)
    {
        if (!empty($model['invoice_id'])) {
            if (Invoice::where('id', $model['invoice_id'])->first() != null) {
                $this->setSubtotal(Invoice::where('id', $model['invoice_id'])->first());
            }
        }
    }
    public function setSubtotal(Invoice $Invoice)
    {
        $d_subtotal = 0;
        $outer_tax = 0.00;
        $inner_tax = 0.00;
        foreach ($Invoice->Shipments as $shipment) {
            foreach ($shipment->ShipmentLineItems as $ShipmentLineItem) {
                $d_subtotal +=$ShipmentLineItem->quantity * $ShipmentLineItem->unit_price;

                if (trim($ShipmentLineItem->TaxType) == '外税') {
                    $outer_tax += $ShipmentLineItem->tax_amount;
                } else {
                    $inner_tax += $ShipmentLineItem->tax_amount;
                }
            }
        }

        $d_subtotal =floor( floor($d_subtotal) - floor($inner_tax));
        $inner_tax = floor($inner_tax);
        $outer_tax = floor($outer_tax);
        $Invoice->subtotal = $d_subtotal;
        $Invoice->tax = $outer_tax + $inner_tax;
        $Invoice->save();
    }
}